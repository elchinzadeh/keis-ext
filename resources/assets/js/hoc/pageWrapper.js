import React, {Component} from 'react';
import {Layout} from 'antd';
import AppHeader from '../components/partials/appHeader';
import AppFooter from '../components/partials/appFooter';

const {Content} = Layout;

export default class PageWrapper extends Component {

    render() {
        return (
            <Layout className="layout">
                <AppHeader currentRoute={this.props.currentRoute}/>
                <Content style={{padding: '1rem 1rem'}}>
                    {this.props.children}
                </Content>
                <AppFooter/>
            </Layout>
        );
    }
}