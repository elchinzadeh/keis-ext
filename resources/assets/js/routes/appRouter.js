import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import AsyncAttendance from '../pages/attendance/asyncAttendance';
import AsyncDashboard from '../pages/dashboard/asyncDashboard';
import AsyncHome from '../pages/home/asyncHome';
import AsyncRegistration from '../pages/home/registration/asyncRegistration';
import AsyncProfile from '../pages/profile/asyncProfile';
import AsyncTeamLeaderPanel from '../pages/teamLeaderPanel/asyncTeamLeaderPanel';
import NotFound from '../pages/notFound/notFound';

import CheckToken from '../components/general/checkToken/checkToken';

import ExtLogin from '../ext-components/login'

const AppRouter = () => {
    return (
        <Switch>
            <Route exact path="/" render={() => (
                <Redirect to="/dashboard"/>
            )}/>
            {/*<Route exact path="/login" render={() => (*/}
                {/*<Redirect to="/auth/login"/>*/}
            {/*)}/>*/}
            <CheckToken exact path="/login" component={AsyncHome}/>
            <CheckToken path="/registration" component={AsyncRegistration}/>
            <CheckToken path="/attendance" component={AsyncAttendance}/>
            <CheckToken path="/dashboard" component={AsyncDashboard}/>
            <CheckToken path="/profile" component={AsyncProfile}/>
            <CheckToken path="/teamLeaderPanel" component={AsyncTeamLeaderPanel}/>
            <Route path="/auth/loginForAsanKadr" component={ExtLogin}/>
            <Route component={NotFound}/>
        </Switch>
    );
};

export default AppRouter;
