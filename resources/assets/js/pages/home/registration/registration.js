import React from 'react';
import { Layout, Card } from 'antd';
import RegistrationForm from '../../../components/home/register/registrationForm'

export default class Registration extends React.Component {
    render () {
        return (
            <div className="page-wrapper registration">
                <Card bordered={false} className="reg-header">
                    <div className="reg-header-content">
                        <h1>ASAN Könüllülük Məktəbi</h1>
                        <p>ASAN School-un məqsədi Azərbaycanda könüllü fəaliyyətinin təkmilləşdirilməsidən və potensial kadr təminatını həyata keçirməkdən ibarətdir.</p>
                    </div>
                </Card>
                <Layout style={{padding: '1rem 1rem'}}>
                    <RegistrationForm/>
                </Layout>
                <Layout.Footer style={{textAlign: 'center'}}>
                    <h3>Azərbaycan Respublikasının Prezidenti yanında Vətəndaşlara Xidmət və Sosial İnnovasiyalar üzrə Dövlət Agentliyi</h3>
                </Layout.Footer>
            </div>
        )
    }
}