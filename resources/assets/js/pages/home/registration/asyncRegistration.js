import Loadable from "react-loadable";
import Loading from "../../../components/general/loading/loading";

const AsyncRegistration = Loadable({
    loader: () => import(/* webpackChunkName: "Registration" */ "./registration"),
    loading: Loading
});

export default AsyncRegistration