import Loadable from "react-loadable";
import Loading from "../../components/general/loading/loading";

const AsyncHome = Loadable({
    loader: () => import(/* webpackChunkName: "Home" */ "./home"),
    loading: Loading
});

export default AsyncHome