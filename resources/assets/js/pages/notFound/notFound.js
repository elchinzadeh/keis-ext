import React, {Component} from 'react';
import { Layout, Button } from 'antd';
import {Link} from 'react-router-dom';
import AppFooter from '../../components/partials/appFooter';

const {Content} = Layout;

export default class  NotFound extends Component {

    render() {

        return (
            <Layout className="layout">
                <Content style={{padding: '1rem 1rem', minHeight: '700px'}}>
                    <div className="error-container">
                        <h1>404</h1>
                        <p className="text-message">Axtardığınız səhifə tapılmadı.</p>
                        <p className="text-details">İstifadə etdiyiniz URL yanlışdır və ya resurs silinmişdir.</p>
                        <Link to="/dashboard">
                            <Button size="large" type="primary">Əsas səhifəyə qayıt</Button>
                        </Link>
                    </div>
                </Content>
                <AppFooter/>
            </Layout>
        );

    }

}