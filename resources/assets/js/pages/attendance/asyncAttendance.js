import Loadable from "react-loadable";
import Loading from "../../components/general/loading/loading";

const AsyncAttendance = Loadable({
    loader: () => import(/* webpackChunkName: "Attendance" */ "./attendance"),
    loading: Loading
});

export default AsyncAttendance