import Loadable from "react-loadable";
import Loading from "../../components/general/loading/loading";

const AsyncProfile = Loadable({
    loader: () => import(/* webpackChunkName: "Profile" */ "./profile"),
    loading: Loading
});

export default AsyncProfile