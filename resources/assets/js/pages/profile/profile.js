import React, {Component} from 'react';
import {Layout, Tabs} from 'antd';

import PageWrapper from '../../hoc/pageWrapper';
import PersonalInfo from '../../components/profile/personal/personalInfo';
import EducationInfo from '../../components/profile/education/educationInfo';
import ExperienceInfo from '../../components/profile/experience/experienceInfo';
import SkillInfo from '../../components/profile/skill/skillInfo';
import FamilyInfo from '../../components/profile/family/familyInfo';
import TrainingInfo from '../../components/profile/training/trainingInfo';
import ProfileImage from '../../components/profile/profileImage/profileImage'

const TabPane = Tabs.TabPane;
const {Content} = Layout;

export default class Profile extends Component {

    render() {

        return (
            <PageWrapper currentRoute={this.props.location.pathname}>
                <Layout style={{background: '#fff', padding: '1em'}}>
                    <Content style={{padding: '0 1rem', minHeight: 540}}>
                        <div className="profile-top">
                            <div className="profile-image">
                                <ProfileImage />
                            </div>
                        </div>

                        <Tabs defaultActiveKey="1" style={{textAlign: 'center'}}>

                            <TabPane tab="Şəxsi məlumatları" key="1">
                                <PersonalInfo/>
                            </TabPane>

                            <TabPane tab="Təhsil məlumatları" key="2">
                                <EducationInfo/>
                            </TabPane>

                            <TabPane tab="Təcrübə" key="3">
                                <ExperienceInfo/>
                            </TabPane>

                            <TabPane tab="Təlim" key="4">
                                <TrainingInfo/>
                            </TabPane>

                            <TabPane tab="Bilik və bacarıqlar" key="5">
                                <SkillInfo/>
                            </TabPane>

                            <TabPane tab="Ailə tərkibi" key="6">
                                <FamilyInfo/>
                            </TabPane>

                        </Tabs>
                    </Content>
                </Layout>
            </PageWrapper>
        );

    }

}

