import Loadable from "react-loadable";
import Loading from "../../components/general/loading/loading";

const AsyncDashboard = Loadable({
    loader: () => import(/* webpackChunkName: "Dashboard" */ "./dashboard"),
    loading: Loading
});

export default AsyncDashboard