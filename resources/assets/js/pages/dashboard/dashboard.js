import React, {Component} from 'react';
import { Card, Steps, Icon} from 'antd';

import PageWrapper from '../../hoc/pageWrapper';
import { DashboardContext } from '../../contexts/dashboardContext';

// APIs
import GeneralApi from "../../api/master1/generalApi";

// Steps
import RegOkNC from "../../components/dashboard/appeal/steps/regOkNC";
import RegOk from "../../components/dashboard/appeal/steps/regOk";
import TestFirstFailed from "../../components/dashboard/appeal/steps/testFirstFailed";
import TestFailed from "../../components/dashboard/appeal/steps/testFailed";
import TestChance from "../../components/dashboard/appeal/steps/testChance";
import TestSuccess from "../../components/dashboard/appeal/steps/testSuccess";
import InterviewCenterAndShiftSelected from "../../components/dashboard/appeal/steps/interviewCenterAndShiftSelected";
import InterviewDateSelected from "../../components/dashboard/appeal/steps/interviewDateSelected";

// Process
import Exam from "../../components/general/exam/"

const Step = Steps.Step;

export default class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            status: null,
            statusText: null,
            testStarted: false,

            steps: {
                test: {
                    title: 'Test mərhələsi',
                    icon: 'solution',
                    content: 'TestInfo',
                    status: 'wait',
                    statusMessage: null
                },
                centerAndShift: {
                    title: 'Mərkəz və növbə seçimi',
                    icon: 'environment-o',
                    status: 'wait',
                    statusMessage: null
                },
                interviewDate: {
                    title: 'Müsahibə vaxtı seçimi',
                    icon: 'calendar',
                    status: 'wait',
                    statusMessage: null
                },
                interview: {
                    title: 'Müsahibə',
                    icon: 'team',
                    status: 'wait',
                    statusMessage: null
                }
            }
        };
    };

    componentDidMount() {
        this.getStatus()
    };

    getStatus = () => {
        GeneralApi.getStatus().then(response => {
            if (response.data.error === null) {
                this.setState({
                    status: response.data.data.name
                    // status: 'centerandshiftselected'
                });
                this.setStepStatus();
            }
        }).catch(err => {});
    };

    setStepStatus = () => {
        let steps = this.state.steps,
            status = this.state.status;

        if (status === 'regok' ||
            status === 'testfailed' ||
            status === 'testfirstfailed' ||
            status === 'testchance') {
            steps.test.status = 'process'
        }
        else if (status === 'testsuccess') {
            steps.test.status = 'finish';
            steps.centerAndShift.status = 'process';
        }
        else if (   status === 'centerandshiftselected' ||
                    status === 'interviewdateselected' ||
                    status === 'interviewdatecancelledbysystem' ||
                    status === 'interviewdatecancelledbyuser' ||
                    status === 'interviewdatechanged' ||
                    status === 'interviewfailed' ||
                    status === 'interviewdidntcame' ||
                    status === 'interviewchance') {
            steps.test.status = 'finish';
            steps.centerAndShift.status = 'finish';
            steps.interviewDate.status = 'process';
        }
        else if (status === 'candidate') {
            steps.test.status = 'finish';
            steps.centerAndShift.status = 'finish';
            steps.interviewDate.status = 'finish';
            steps.interviewDate.status = 'process';
        }

        this.setState({steps: steps})
    };

    startTest = () => {
        this.setState({
            testStarted: true
        })
    };

    render() {

        const components = {
            'regoknc'                           : <RegOkNC/>,
            'regok'                             : (this.state.testStarted ? <Exam/> : <RegOk startTest={this.startTest}/>),
            'testfirstfailed'                   : (this.state.testStarted ? <Exam/> : <TestFirstFailed startTest={this.startTest}/>),
            'testfailed'                        : <TestFailed/>,
            'testchance'                        : (this.state.testStarted ? <Exam/> : <TestChance startTest={this.startTest}/>),
            'testsuccess'                       : <TestSuccess getStatus={this.getStatus}/>,
            'centerandshiftselected'            : <InterviewCenterAndShiftSelected/>,
            'interviewdateselected'             : <InterviewDateSelected/>,
            'interviewdatecancelledbysystem'    : 'interview-datecancelledbysystem',
            'interviewdatecancelledbyuser'      : 'interview-datecancelledbyuser',
            'interviewdatechanged'              : 'interview-datechanged',
            'interviewfailed'                   : 'interview-failed',
            'interviewdidntcame'                : 'interview-didntcame',
            'interviewchance'                   : 'interview-chance',
            'candidate'                         : 'interview-success',
        };

        return (
            <PageWrapper currentRoute={this.props.location.pathname}>
                <DashboardContext.Provider value={{
                    status: this.state.status,
                    getStatus: this.getStatus
                }}>
                    <Card bordered={false} className="card">
                        <Steps>
                            {Object.entries(this.state.steps).map(([key, value]) => {
                                return <Step key={key} status={value.status} title={value.title} icon={<Icon type={value.icon} />} />
                            })}
                        </Steps>
                    </Card>

                    { components[this.state.status] }
                </DashboardContext.Provider>
            </PageWrapper>
        );
    }
}