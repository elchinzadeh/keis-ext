import React from 'react'
import {Card} from "antd";
import PageWrapper from "../../hoc/pageWrapper";
import FirstAssessment from '../../components/teamLeaderPanel/firstAssessment'
import SecondAssessment from "../../components/teamLeaderPanel/secondAssessment";

export default class TeamLeaderPanel extends React.Component {
    render() {
        return(
            <PageWrapper currentRoute={this.props.location.pathname}>
                <Card bordered={false} style={{marginBottom: 16}}>
                    <h2>Birinci qiymətləndirmə</h2>
                    <FirstAssessment/>
                </Card>
                <Card bordered={false}>
                    <h2>İkinci qiymətləndirmə</h2>
                    <SecondAssessment/>
                </Card>
            </PageWrapper>
        )
    }
}