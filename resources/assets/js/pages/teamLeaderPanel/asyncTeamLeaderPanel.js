import Loadable from "react-loadable";
import Loading from "../../components/general/loading/loading";

const AsyncTeamLeaderPanel = Loadable({
    loader: () => import(/* webpackChunkName: "TeamLeaderPanel" */ "./teamLeaderPanel"),
    loading: Loading
});

export default AsyncTeamLeaderPanel