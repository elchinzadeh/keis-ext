import service from '../service'

export default class TestApi{
    static getTest() {
        return service.get('master1/process/test/getTest')
    }

    static checkActiveTest() {
        return service.get('master1/process/test/checkActiveTest')
    }

    static submitTest(body) {
        return service.post('master1/process/test/submitTest', body)
    }
}