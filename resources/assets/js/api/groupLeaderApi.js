import service from './service'

export default class GroupLeaderApi{
    static getFirstAssessment() {
        return service.get('groupLeader/getFirstAssesment')
    }

    static getSecondAssessment() {
        return service.get('groupLeader/getSecondAssesment')
    }

    static getSingleAssessment(id) {
        return service.get('groupLeader/getSingleAssesment/' + id)
    }

    static submitFirstAssesment(body) {
        return service.post('groupLeader/submitFirstAssesment', body)
    }

    static submitSecondAssesment(body) {
        return service.post('groupLeader/submitSecondAssesment', body)
    }
}