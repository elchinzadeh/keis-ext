import cookie from 'react-cookies';

const isToLogin = [1004, 1005];

let protocol = window.location.protocol,
    host = window.location.host,
    token = cookie.load('ut') ? 'Basic ' + cookie.load('ut') : 'Basic null',
    ipAddress = null;

const service = axios.create({
    baseURL: protocol + '//' + host + '/api?uri=',
    timeout: 10000,
    headers: {
        'Access-Control-Allow-Origin': protocol + '//' + host,
        'Accept-Language': 'az',
        'Authorization': token,
        'Content-Type': 'application/json',
        // 'IpAddress': ipAddress,
    },
    // transformResponse: axios.defaults.transformResponse.concat((data) => {
    //     if (data.error != null) {
    //         if (isToLogin.includes(data.error.code)) {
    //             console.log('to login');
    //             cookie.remove('ut', { path: '/' });
    //             window.location.replace('/auth/login');
    //         }
    //     }
    //     return data
    // })
});

export default service;