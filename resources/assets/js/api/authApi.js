import service from './service'

export default class AuthApi {
    static login(body) {
        return service.post('auth/login', body)
    }

    static logout() {
        return service.get('auth/logout')
    }

    static checkToken() {
        return service.get('auth/checkToken')
    }
}