import service from '../service';

export default class EducationApi {

    static getEducationInfo() {
        return service.get('profile/eduInfo/get');
    }

    static insertEducationInfo(body) {
        return service.post('profile/eduInfo/insert', body);
    }

    static updateEducationInfo(body) {
        return service.post('profile/eduInfo/update', body);
    }

    static deleteEducationInfo(id) {
        return service.get('profile/eduInfo/delete/'+id);
    }
}