import service from '../service'

export default class FamilyApi {

    static getFamilyInfo() {
        return service.get('profile/familyInfo/get')
    }

    static getById(id) {
        return service.get('profile/familyInfo/getById/' + id)
    }

    static insertFamilyInfo(body) {
        return service.post('profile/familyInfo/insert', body)
    }

    static updateFamilyInfo(body) {
        return service.post('profile/familyInfo/update', body)
    }

    static deleteFamilyInfo(id) {
        return service.get('profile/familyInfo/delete/'+id)
    }
}