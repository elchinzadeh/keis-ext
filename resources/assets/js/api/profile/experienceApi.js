import service from '../service';

export default class ExperienceApi {

    static getExperienceInfo() {
        return service.get('profile/expInfo/get');
    }

    static insertExperienceInfo(body) {
        return service.post('profile/expInfo/insert', body);
    }

    static updateExperienceInfo(body) {
        return service.post('profile/expInfo/update', body);
    }

    static deleteExperienceInfo(id) {
        return service.get('profile/expInfo/delete/'+id);
    }
}