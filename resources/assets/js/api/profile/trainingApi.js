import service from '../service';

export default class TrainingApi {

    static getTrainingInfo() {
        return service.get('profile/trainingInfo/get');
    }

    static getTrainingInfoById(id) {
        return service.get('profile/trainingInfo/getById/' + id);
    }

    static insertTrainingInfo(body) {
        return service.post('profile/trainingInfo/insert', body);
    }

    static updateTrainingInfo(body) {
        return service.post('profile/trainingInfo/update', body);
    }

    static deleteTrainingInfo(id) {
        return service.get('profile/trainingInfo/delete/'+id);
    }
}