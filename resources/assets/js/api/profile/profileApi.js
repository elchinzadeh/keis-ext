import service from '../service';

export default class ProfileApi {

    static getPersonalInfo() {
        return service.get('profile/personalInfo/get');
    }

    static updatePersonalInfo(body) {
        return service.post('profile/personalInfo/updatePersonalInfo', body);
    }

    static updateImage(body) {
        return service.post('profile/personalInfo/updateImage', body);
    }
}