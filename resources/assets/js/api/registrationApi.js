import service from './service'

export default class RegistrationApi {
    static getIdCardData(idCardNo ,idCardFin) {
        return service.get('registration/getIdCardData/' + idCardNo +'/'+ idCardFin)
    }

    static emailConfirmation(config) {
        return service.get('registration/emailConfirmation', config)
    }

    static registration(body) {
        return service.post('registration/register', body)
    }

    static resendEmailConfirmation() {
        return service.get('registration/resendEmailConfirmation')
    }
}