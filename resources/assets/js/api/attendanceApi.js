    import service from './service';

export default class AttendanceApi {

    static get () {
        return service.get('attendance/get')
    }
}