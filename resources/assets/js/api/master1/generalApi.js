import service from '../service'

export default class GeneralApi {
    static getStatus() {
        return service.get('master1/general/getStatus')
    }

    static getUserM1() {
        return service.get('master1/general/getUserM1')
    }
}