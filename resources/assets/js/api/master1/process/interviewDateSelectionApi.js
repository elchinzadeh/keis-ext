import service from '../../service';

export default class InterviewDateSelectionApi {
    static getInterviewSchedules() {
        return service.get('master1/process/interviewDateSelection/getInterviewSchedules')
    }

    static submitSchedule(body) {
        return service.post('master1/process/interviewDateSelection/submitSchedule', body)
    }
}