import service from '../../service';

export default class InterviewApi {
    static getInfo() {
        return service.get('master1/process/interview/getInfo')
    }
}