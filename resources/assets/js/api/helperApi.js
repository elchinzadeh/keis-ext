import service from './service';

export default class HelperApi {

    static getCenterTypes() {
        return service.get('helpers/listCenterType/getAll');
    }

    static getEducationOrganizations() {
        return service.get('helpers/listEducationOrganization/getAll');
    }

    static getProfessions() {
        return service.get('helpers/listProfession/getAll');
    }

    static getEducationLevels() {
        return service.get('helpers/listEducationLevel/getAll');
    }

    static getKinshipTypes() {
        return service.get('helpers/listKinshipType/getAll')
    }

    static getlistCountry() {
        return service.get('helpers/listCountry/getAll');
    }

    static getlistSkillType() {
        return service.get('helpers/listSkillType/getAll');
    }

    static getKnowledgeLevels() {
        return service.get('helpers/listKnowledgeLevel/getAll')
    }

    static getDistrictCities() {
        return service.get('helpers/listDistrictCity/getAll')
    }

    static getJobAreas() {
        return service.get('helpers/listJobArea/getAll');
    }

    static getListMusic() {
        return service.get('helpers/listMusic/getAll')
    }

    static getListSport() {
        return service.get('helpers/listSport/getAll')
    }

    static getListLanguage() {
        return service.get('helpers/listLanguage/getAll')
    }
}