import service from './service'

export default class FileUploadApi {
    static upload(body) {
        return service.post('fileTransfer/upload', body)
    }

    static get(config) {
        return service.get('fileTransfer/get', config)
    }
}