import React, { Component } from 'react';
import {Row, Col, Card, Button, Tooltip, Skeleton, message, Icon} from 'antd';
import moment from 'moment';

import InterviewDateSelectionApi from "../../../../api/master1/process/interviewDateSelectionApi";

export default class SelectCenterShiftForm extends Component{
    constructor() {
        super();

        this.state = {
            data: [],
            selectedTimeId: null,
            loading: false
        }
    }

    componentDidMount() {
        this.getInterviewDates()
    }

    getInterviewDates = () => {
        this.setState({loading: true});
        InterviewDateSelectionApi.getInterviewSchedules().then(response => {
            if (!response.data.error){
                this.setState({
                    data: response.data.data,
                    loading: false
                })
            }else {
                message.error(response.data.error.message);
                this.setState({loading: false});
            }
        }).catch(error => {
            message.error(error.message);
            this.setState({loading: false});
        })
    };

    setSelectedTime = id => {
        this.setState({
            selectedTimeId: id
        })
    };

    handleSubmit = () => {
        const body = {
            id: this.state.selectedTimeId
        };

        InterviewDateSelectionApi.submitSchedule(body).then(response => {
            if (!response.data.error){
                this.props.getStatus()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    render() {
        return (
            <div className="InterviewDates">
                <Skeleton active loading={this.state.loading}>
                    <h2>Müsahibə vaxtını seçin</h2>
                    <Row>
                        {
                            this.state.data ? this.state.data.map(intDay => {
                                return intDay.interviewSchedules ?
                                    <Col key={intDay.date} xl={24} md={24}>
                                        <Card className="interview-day-card" title={moment(intDay.date).format('LL')}>
                                            {
                                                intDay.interviewSchedules.map(intTime => {
                                                    return  <Tooltip key={intTime.id} title={!intTime.isEmpty ? 'Bu vaxt artıq seçilib' : null}>
                                                        <Button className="interview-time-btn"
                                                                type={this.state.selectedTimeId === intTime.id ? 'primary' : null}
                                                                disabled={!intTime.isEmpty}
                                                                onClick={() => this.setSelectedTime(intTime.id)}
                                                        >
                                                            {moment(intTime.time, 'HH:mm:ss').format('HH:mm')}
                                                        </Button>
                                                    </Tooltip>
                                                })
                                            }
                                        </Card>
                                    </Col>
                                    : null
                            }) : null
                        }
                    </Row>
                    {
                        this.state.data ? <h4>Müsahibəyə seçilən vaxtdan öncə gəlməyi unutmayın <Icon type="smile-o"/></h4> : null
                    }

                    <Button type="primary" onClick={this.handleSubmit}>Davam et</Button>
                </Skeleton>
            </div>

        )
    }
}