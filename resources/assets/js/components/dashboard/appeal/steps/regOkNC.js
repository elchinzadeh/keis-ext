import React, { Component } from 'react';
import { Card, Icon, message } from 'antd';
import RegistrationApi from "../../../../api/registrationApi";

export default class RegOkNC extends Component{

    resendEmailConfirmation = () => {
        RegistrationApi.resendEmailConfirmation().then(response => {
            if (!response.data.error){
                message.success('Məktub göndərildi')
            } else {
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    render() {
        return(
            <Card className="card content" bordered={false} align="center">
                <h1><Icon type="smile-o"/></h1>
                <h2>Zəhmət olmasa emailinizi təsdiqləyin</h2>
                <p>E-poçtunuza təsdiq məktubu gəlməyib? <a onClick={this.resendEmailConfirmation}>Yenidən göndər</a></p>
            </Card>
        )
    }
}