import React, { Component } from 'react';
import { Card, Icon} from 'antd';

import SelectCenterShift from '../selectCenterShift/selectCenterShift'

export default class TestSuccess extends Component{
    render() {

        return(
            <Card className="card" bordered={false} align="center">
                <h1><Icon type="smile-o"/></h1>
                <h2>Siz imtahan mərhələsində uğurlu oldunuz</h2>

                <p>Könüllü olmaq istədiyiniz mərkəzi və növbəni seçin</p>
                <SelectCenterShift getStatus={this.props.getStatus}/>
            </Card>
        )
    }
}