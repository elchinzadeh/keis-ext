import React, { Component } from 'react';
import { Card, Button, Icon} from 'antd';

import SelectInterviewDate from '../selectInterviewDate/selectInterviewDate';
import {DashboardContext} from "../../../../contexts/dashboardContext";

export default class InterviewCenterAndShiftSelected extends Component{
    state = {
        showSelection: false
    };

    render() {
        return(
            <Card className="card" bordered={false} align="center">
                <h1><Icon type="smile-o"/></h1>
                {
                    !this.state.showSelection ?
                        <React.Fragment>
                            <h2>Mərkəz və növbə seçimi tamamladı</h2>
                            <div>Müsahibə vaxtını seçmək üçün davam edin</div>
                            <br/>
                            <Button type="primary" onClick={() => this.setState({showSelection: true})}>Davam et</Button>
                        </React.Fragment>
                    :
                        <DashboardContext.Consumer>
                            {value => <SelectInterviewDate getStatus={value.getStatus}/>}
                        </DashboardContext.Consumer>
                }
            </Card>
        )
    }
}