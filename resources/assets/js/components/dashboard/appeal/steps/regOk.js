import React, { Component } from 'react';
import { Card, Icon, Button } from 'antd';
import TestApi from "../../../../api/general/testApi";

export default class RegOk extends Component{

    constructor() {
        super();

        this.state = {
            message: null,
            fetching: false
        };
    }

    componentDidMount() {
        this.checkActiveTest()
    }

    checkActiveTest = () => {
        this.setState({
            fetching: true
        });

        TestApi.checkActiveTest().then(response => {
            if(response.data.error === null) {
                if(response.data.data === 'yes') {
                    this.showTest();
                }else if(response.data.data === 'no') {
                    this.setState({
                        fetching: false
                    })
                }else if(response.data.data === 'expired') {
                    //
                }
            }
        });
    };

    showTest = () => {
        this.setState({
            fetching: false
        });
        this.props.startTest();
    };

    render() {
        return(
            <Card className="card content" bordered={false} align="center" loading={this.state.fetching}>
                <h1><Icon type="smile-o"/></h1>
                <h2>Siz qeydiyyatdan uğurla keçdiniz!</h2>
                <p>Test mərhələsinə keçmək üçün davam edin</p>
                <Button type="primary" onClick={this.showTest}>Testə başla</Button>
            </Card>
        )
    }
}