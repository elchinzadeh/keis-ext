import React, { Component } from 'react';
import {Card, Button, Icon, Progress, message} from 'antd';
import moment from "moment";
import InterviewApi from "../../../../api/master1/process/interviewApi";


export default class InterviewDateSelected extends Component{
    constructor(){
        super();

        this.state = {
            data: null
        }
    }
    componentDidMount() {
        this.getInterviewInfo()
    }

    getInterviewInfo(){
        InterviewApi.getInfo().then(response => {
            if (!response.data.error){
                this.setState({
                    data: response.data.data,
                })
            }else {
                message.error(response.data.error.message);
            }
        }).catch(error => {
            message.error(error.message);
        })
    }

    render() {

        return(
            <Card className="card" bordered={false} align="center">
                {
                    this.state.data ?
                        <React.Fragment>
                            <h1><Icon type="smile-o"/></h1>
                            <h2>Sizin {moment(this.state.data.date).format('DD MMMM')} {moment(this.state.data.time, 'HH:mm:ss').format('H:mm')} tarixində müsahibəniz var</h2>
                            <Progress type="circle" style={{marginBottom: '1rem'}} percent={99.99} format={() => `${moment(this.state.data.date).from(moment(), true)}`} />
                            <h2>{this.state.data.districtCityId.name +', '+ this.state.data.centerName}</h2>
                            <p>Müsahibəyə seçilən vaxtdan öncə gəlməyiniz xahiş olunur</p>

                            <Button type="primary">Yaxşı</Button>
                        </React.Fragment>
                        : null
                }
            </Card>
        )
    }
}