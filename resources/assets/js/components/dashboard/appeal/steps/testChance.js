import React, { Component } from 'react';
import { Card, Button, Icon } from 'antd';
import TestApi from "../../../../api/general/testApi";

export default class TestChance extends Component{

    constructor() {
        super();

        this.state = {
            fetching: false
        };
    }

    componentDidMount() {
        this.checkActiveTest()
    }

    checkActiveTest = () => {
        this.setState({
            fetching: true
        });

        TestApi.checkActiveTest().then(response => {
            if(response.data.error === null) {
                if(response.data.data === 'yes') {
                    this.showTest();
                }else if(response.data.data === 'no') {
                    this.setState({
                        fetching: false
                    })
                }else if(response.data.data === 'expired') {
                    //
                }
            }
        });
    };

    showTest = () => {
        this.setState({
            fetching: false
        });
        this.props.startTest();
    };

    render()    {
        return(
            <Card className="card" bordered={false} align="center" loading={this.state.fetching}>
                <h1><Icon type="smile-o"/></h1>
                <h2>Sizə imtahan üçün bir şans daha verildi</h2>
                <Button type="primary" onClick={this.showTest}>Testə başla</Button>
            </Card>
        )
    }
}