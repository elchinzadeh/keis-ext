import React, { Component } from 'react';
import { Col, Button, Icon, Form, Select, Radio, message } from 'antd';
import moment from 'moment'
import CenterAndShiftSelectionApi from "../../../../api/master1/process/centerAndShiftSelectionApi";

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class SelectCenterShiftForm extends Component{
    constructor(props){
        super(props);

        this.state = {
            centerList: [],
            shiftList: [],
            selectedShift: null,
            shiftsLoading: false
        }
    }

    componentDidMount(){
        this.getCenterList()
    }

    getCenterList = () => {
        CenterAndShiftSelectionApi.getCenters().then(response => {
            if (response.data.error === null) {
                this.setState({
                    centerList: response.data.data.entities
                })
            }else{
                message.error(response.data.error.message)
            }
        })
    };

    getShiftList = (id) => {
        this.setState({shiftsLoading: true});
        CenterAndShiftSelectionApi.getShiftsByCenterId(id).then(response => {
            if (response.data.error === null){
                this.setState({
                    shiftList: response.data.data.entities,
                    shiftsLoading: false
                })
            }else{
                message.error(response.data.error.message);
                this.setState({shiftsLoading: true});
            }
        })
    };

    handleShiftChange = (e) => {
        this.setState({
            selectedShift: e.target.value
        })
    };

    handleSubmit = () => {
        let body = {
            relCenterShiftId: this.state.selectedShift
        };

        CenterAndShiftSelectionApi.submitCenterAndShift(body).then(response => {
            if (response.data.error === null){
                message.success('Mərkəz və növbə seçimi tamamlandı');
                this.props.getStatus();
            }else{
                message.error(response.data.error.message)
            }
        })
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const radioStyle = {
            display: 'block',
            lineHeight: '30px',
        };
        return(
            <Col className="mt-30" lg={{ span: 12, offset: 6}} md={{ span: 16, offset: 4}}>
                <Form>
                    <FormItem>
                        {getFieldDecorator('center', {
                            rules: [{ required: true }],
                        })(
                            <Select
                                showSearch
                                placeholder="Mərkəzi seçin"
                                optionFilterProp="name"
                                onChange={value => this.getShiftList(value)}
                            >
                                {this.state.centerList.map((value) => {
                                    return <Option value={value.id} name={value.name} key={value.id}>{value.name}</Option>
                                })}
                            </Select>
                        )}
                    </FormItem>
                    {
                        this.state.shiftsLoading ? <h1><Icon type="loading" /></h1> : null
                    }
                    <FormItem className="align-left">
                        {getFieldDecorator('shift', {
                            rules: [{ required: true }],
                        })(
                            this.state.shiftList ?
                                <RadioGroup onChange={this.handleShiftChange}>
                                    {
                                        this.state.shiftList.map(item => {
                                            return  <Radio key={item.id} style={radioStyle} value={item.id}>
                                                        <b>{item.name}</b>
                                                        <div style={{marginLeft: 25}}>
                                                            Həftə içi: {moment(item.weekdaysStartTime, 'HH:mm:ss').format('H:mm')} - {moment(item.weekdaysEndTime, 'HH:mm:ss').format('H:mm')}<br/>
                                                            Şənbə: {moment(item.weekdaysStartTime, 'HH:mm:ss').format('H:mm')} - {moment(item.weekdaysEndTime, 'HH:mm:ss').format('H:mm')}<br/>
                                                            Bazar: {moment(item.weekdaysStartTime, 'HH:mm:ss').format('H:mm')} - {moment(item.weekdaysEndTime, 'HH:mm:ss').format('H:mm')}<br/>
                                                        </div>
                                                    </Radio>
                                        })
                                    }
                                </RadioGroup>
                            : null
                        )}
                    </FormItem>
                    <FormItem>
                        <Button type="primary" onClick={this.handleSubmit} disabled={!this.state.selectedShift}>
                            Təsdiqlə
                        </Button>
                    </FormItem>
                </Form>
            </Col>
        )
    }
}

const SelectCenterShift = Form.create()(SelectCenterShiftForm);

export default SelectCenterShift;