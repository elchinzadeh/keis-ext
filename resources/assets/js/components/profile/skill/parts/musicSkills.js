import React from 'react';
import {Col, Row, Button, Icon, List, Divider, Popconfirm, message} from 'antd';
import SkillApi from "../../../../api/profile/skillApi";
import MusicSkillModal from '../modals/musicSkillModal'

export default class MusicSkills extends React.Component {

    constructor() {
        super();

        this.state = {
            showModal: false,
            selectedEntity: null,
        }
    }

    toggleModal = () => {
        this.setState({
            showModal: !this.state.showModal
        })
    };

    clearSelectedEntity = () => {
        this.setState({
            selectedEntity: null
        })
    };

    saveForm = (formRef) => {
        this.formRef = formRef;
    };

    handleSubmit = () => {
        const form = this.formRef.props.form;

        form.validateFields((err, values) => {
            if (!err) {
                if (this.state.selectedEntity === null) {
                    this.insertSkill(values);
                } else {
                    this.updateSkill(values);
                }
            }


        });
    };

    editSkill = (value) => {
        this.setState({
            selectedEntity: value,
            showModal: true
        })
    };

    updateSkill = (values) => {
        let body = {
            id: this.state.selectedEntity.id,
            musicId: {id: values.musicId},
            knowledgeLevelId: {id: values.knowledgeLevelId},
            note: values.note
        };

        SkillApi.updateMusicSkill(body).then(response => {
            if (response.data.error === null) {
                this.toggleModal();
                this.props.getSkillInfo();
                message.success('Məlumat yeniləndi')
            }else{
                message.error(response.data.error.message)
            }
        })
    };

    insertSkill = (values) => {
        let body = {
            musicId: {id: values.musicId},
            knowledgeLevelId: {id: values.knowledgeLevelId},
            note: values.note
        };

        SkillApi.insertMusicSkill(body).then(response => {
            if (response.data.error === null) {
                this.toggleModal();
                this.props.getSkillInfo();
                message.success('Məlumat bazaya əlavə olundu')
            }else{
                message.error(response.data.error.message)
            }
        })
    };

    deleteSkill = (id) => {
        SkillApi.delete(id).then(response => {
            if (response.data.error === null) {
                this.props.getSkillInfo();
                message.success('Məlumat bazadan silindi')
            }else{
                message.error(response.data.error.message)
            }
        })
    };

    render() {
        return(
            <React.Fragment>
                {this.state.showModal ?
                    <MusicSkillModal
                        showModal={this.state.showModal}
                        selectedEntity={this.state.selectedEntity}
                        handleOk={this.handleSubmit}
                        handleCancel={this.toggleModal}
                        clearSelectedEntity={this.clearSelectedEntity}
                        wrappedComponentRef={this.saveForm}
                    /> : null

                }
                <Divider>Musiqi bilikləri</Divider>

                <Col xs={24} sm={24} md={{span: 14, offset: 5}} lg={{span: 14, offset: 5}}>

                    <Row>
                        <Col style={{textAlign: 'right'}}>
                            <Button type="primary" onClick={this.toggleModal}>
                                <Icon type="plus-circle-o"/>Əlavə et
                            </Button>
                        </Col>
                    </Row>

                    <Row>
                        { this.props.skillInfo !== null ?
                            <List dataSource={this.props.skillInfo}
                                  renderItem={item => (
                                      <List.Item actions={[
                                          <a onClick={() => this.editSkill(item)}>Dəyişiklik et</a>,
                                          <Popconfirm title="Bacarıq məlumatını silməyə əminsinizmi?"
                                                      onConfirm={() => this.deleteSkill(item.id)}
                                                      okText="Bəli"
                                                      cancelText="Xeyr">
                                              <a style={{color: 'red'}}>Sil</a>
                                          </Popconfirm>
                                      ]}>

                                          <List.Item.Meta
                                              title={<a onClick={(e) => {
                                                  e.preventDefault();
                                              }}>{item.musicId.name}</a>}
                                              description={item.knowledgeLevelId.name}
                                          />

                                          <div>
                                              {item.note !== null ? item.note : 'Qeyd yoxdur'}
                                          </div>
                                      </List.Item>
                                  )}
                            >
                            </List> : 'Məlumat daxil edilməyib'
                        }

                    </Row>

                </Col>
            </React.Fragment>
        )
    }
};