import React, {Component} from 'react';
import { Card } from 'antd';

import SkillApi from "../../../api/profile/skillApi";

import ItSkills from './parts/itSkills'
import LanguageSkills from "./parts/languageSkills";
import MusicSkills from "./parts/musicSkills";
import SportSkills from "./parts/sportSkills";
import OtherSkills from './parts/otherSkills'

export default class SkillInfo extends Component {
    constructor() {
        super();

        this.state = {
            entities: {}
        }
    }

    componentDidMount() {
        this.getSkillInfo()
    }

    getSkillInfo = () => {
        SkillApi.get().then(response => {
            if (!response.data.error) {
                this.setState({
                    entities: response.data.data
                })
            }
        })
    };

    render() {
        return (
            <Card loading={false}>

                <LanguageSkills
                    getSkillInfo={this.getSkillInfo}
                    skillInfo={this.state.entities.languageSkills}
                />

                <ItSkills
                    getSkillInfo={this.getSkillInfo}
                    skillInfo={this.state.entities.itSkills}
                />

                <MusicSkills
                    getSkillInfo={this.getSkillInfo}
                    skillInfo={this.state.entities.musicSkills}
                />

                <SportSkills
                    getSkillInfo={this.getSkillInfo}
                    skillInfo={this.state.entities.sportSkills}
                />

                <OtherSkills
                    getSkillInfo={this.getSkillInfo}
                    skillInfo={this.state.entities.otherSkills}
                />
            </Card>
        );
    }
};

