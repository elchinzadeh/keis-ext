import React, {Component} from 'react';
import {Card, List, Popconfirm, Button, Badge, Icon, message, Col, Row, Divider} from 'antd';
import moment from 'moment';

import EducationApi from '../../../api/profile/educationApi';
import HelperApi from '../../../api/helperApi';

import EducationForm from './educationForm';


export default class EducationInfo extends Component {

    constructor() {
        super();
        this.state = {
            fetching: false,
            posting: false,
            entities: [],
            selectedEntity: null,
            totalCount: 0,
            modalOpen: false,

            organizations: [],
            levels: [],
            professions: []
        };
    }

    componentDidMount() {
        this.loadEducationInfo();
        this.getOrganizations();
        this.getEducationLevels();
        this.getProfessions();
    }

    getOrganizations = () => {

        HelperApi.getEducationOrganizations().then(response => {

            if (response.data.error === null) {
                this.setState({
                    organizations: response.data.data.entities
                });
            }

        }).catch();

    };

    getEducationLevels = () => {

        HelperApi.getEducationLevels().then(response => {

            if (response.data.error === null) {
                this.setState({
                    levels: response.data.data.entities
                });
            }

        }).catch();

    };

    getProfessions = () => {

        HelperApi.getProfessions().then(response => {

            if (response.data.error === null) {
                this.setState({
                    professions: response.data.data.entities
                });
            }

        }).catch();

    };

    showModal = () => {
        this.setState({modalOpen: true, selectedEntity: null});
    };

    handleCancel = () => {
        this.setState({modalOpen: false});
    };

    handleCreate = () => {

        const form = this.formRef.props.form;

        form.validateFields((err, values) => {

            if (err) {
                return;
            }

            if (this.state.selectedEntity === null) {
                this.insertEducationInfo(values);
            } else {
                this.updateEducationInfo(values);
            }
        });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    loadEducationInfo = () => {

        this.setState({
            fetching: true
        });

        EducationApi.getEducationInfo().then(response => {
            this.setState({
                fetching: false
            });

            if (response.data.error === null) {

                this.setState({
                    entities: response.data.data.entities,
                    totalCount: response.data.data.totalCount
                });

            } else {
                message.error(response.data.error.message);
            }

        }).catch(error => {
            this.setState({
                fetching: false
            });
        });

    };

    editEducationInfo = (item) => {

        this.setState({
            selectedEntity: item,
            modalOpen: true
        });

    };

    updateEducationInfo = (values) => {

        const form = this.formRef.props.form;

        const body = {
            id: this.state.selectedEntity.id,
            educationForm: values.educationForm,
            startDate: values.dateRange[0].format('YYYY-MM-DD'),
            endDate: values.dateRange[1].format('YYYY-MM-DD'),
            educationLevelId: {id: values.educationLevelId},
            educationOrganizationId: {id: values.educationOrganizationId},
            professionId: {id: values.professionId}
        };

        EducationApi.updateEducationInfo(body).then(response => {

            if (response.data.error === null) {
                this.setState({modalOpen: false});
                this.loadEducationInfo();
                form.resetFields();
            } else {
                message.error(response.data.error.message);
            }

        }).catch();

    };

    insertEducationInfo = (values) => {

        const form = this.formRef.props.form;

        const body = {
            educationForm: values.educationForm,
            startDate: values.dateRange[0].format('YYYY-MM-DD'),
            endDate: values.dateRange[1].format('YYYY-MM-DD'),
            educationLevelId: {id: values.educationLevelId},
            educationOrganizationId: {id: values.educationOrganizationId},
            professionId: {id: values.professionId}
        };

        EducationApi.insertEducationInfo(body).then(response => {

            if (response.data.error === null) {
                this.setState({modalOpen: false});
                this.loadEducationInfo();
                form.resetFields();
            } else {
                message.error(response.data.error.message);
            }

        }).catch();

    };

    deleteEducationInfo = (id) => {
        EducationApi.deleteEducationInfo(id).then(response => {

            if (response.data.error === null) {
                this.loadEducationInfo();
            } else {
                message.error(response.data.error.message);
            }
        }).catch();
    };

    render() {

        const educationForms = {1:'Əyani', 2:'Qiyabi', 3:'Distant'};

        return (
            <Card loading={this.state.fetching}>
                <Divider>Təhsil məlumatları</Divider>
                <EducationForm
                    wrappedComponentRef={this.saveFormRef}
                    educationInfo={this.state.selectedEntity}
                    organizations={this.state.organizations}
                    professions={this.state.professions}
                    levels={this.state.levels}
                    visible={this.state.modalOpen}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}/>

                <Row>
                    <Col md={{span: 14, push: 5}} lg={{span: 14, push: 5}} xs={24} sm={24} style={{textAlign: 'right'}}>
                        <Button type="primary" onClick={this.showModal}>
                            <Icon type="plus-circle-o"/>Əlavə et
                        </Button>
                    </Col>
                </Row>

                <Row>
                    <Col md={{span: 14, push: 5}} lg={{span: 14, push: 5}} xs={24} sm={24}>
                        <List
                            loading={this.state.fetching}
                            itemLayout="horizontal"
                            dataSource={this.state.entities}
                            locale={{emptyText: 'Təhsil məlumatı yoxdur :( Əlavə et düyməsinə klikləməklə təhsiliniz haqqında məlumat əldə edə bilərsiniz'}}
                            renderItem={item => (
                                    <List.Item actions={[

                                        <a onClick={() => this.editEducationInfo(item)}>Dəyişiklik et</a>,
                                        <Popconfirm title="Təhil məlumatını silməyə əminsinizmi?"
                                                    onConfirm={() => this.deleteEducationInfo(item.id)}
                                                    okText="Bəli" cancelText="Xeyr">
                                            <a style={{color: 'red'}}>Sil</a>
                                        </Popconfirm>
                                    ]}>

                                    <List.Item.Meta
                                        title={<a onClick={(e) => { e.preventDefault(); }}>{item.educationOrganizationId.name}&nbsp;<Badge count={educationForms[item.educationForm]} style={{backgroundColor: '#52c41a'}}/> </a>}
                                        description={item.professionId.name + ', ' + item.educationLevelId.name}
                                    />
                                    <div>
                                        {moment(item.startDate).format('LL')}
                                        &nbsp;-&nbsp;
                                        {item.endDate === null ? 'Davam edir' : moment(item.endDate).format('LL')}
                                    </div>
                                </List.Item>
                            )}
                        />
                    </Col>
                </Row>

            </Card>
        );
    }
}

