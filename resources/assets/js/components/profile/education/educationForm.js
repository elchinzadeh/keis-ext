import React, {Component} from 'react';
import {Modal, Form, DatePicker, Select, Radio} from 'antd';

import moment from 'moment';

const {RangePicker} = DatePicker;
const FormItem = Form.Item;

class EduForm extends Component {

    render() {

        const {visible, onCancel, onCreate, form, educationInfo, levels, organizations, professions} = this.props;

        const {getFieldDecorator} = form;

        const formItemLayout = {
            labelCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 6
                },
                lg: {
                    span: 6
                }
            },
            wrapperCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 18
                },
                lg: {
                    span: 18
                }
            }
        };

        return (
            <Modal
                visible={visible}
                title={educationInfo === null ? 'Təhsil məlumatının əlavə edilməsi' : 'Təhsil məlumatının redaktəsi'}
                okText="Təsdiq"
                cancelText="İmtina"
                onCancel={onCancel}
                onOk={onCreate}>

                <Form layout="horizontal">

                    <FormItem label="Təhsil müəsisəsi" {...formItemLayout}>
                        {getFieldDecorator('educationOrganizationId', {
                            initialValue: educationInfo !== null ? educationInfo.educationOrganizationId.id : null,
                            rules: [{required: true, message: 'Zəhmət olmasa təhsil müəsisəsini seçin'}],
                        })(
                            <Select showSearch={true}
                                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                    style={{width: '100%'}}>
                                {
                                    organizations.map(item => {
                                        return <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                                    })
                                }
                            </Select>
                        )}
                    </FormItem>

                    <FormItem label="Təhsil səviyyəsi" {...formItemLayout}>
                        {getFieldDecorator('educationLevelId', {
                            initialValue: educationInfo !== null ? educationInfo.educationLevelId.id : null,
                            rules: [{required: true, message: 'Zəhmət olmasa təhsil səviyyəsini seçin'}],
                        })(
                            <Select showSearch={true}
                                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                    style={{width: '100%'}}>
                                {
                                    levels.map(item => {
                                        return <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                                    })
                                }
                            </Select>
                        )}
                    </FormItem>

                    <FormItem label="İxtisas" {...formItemLayout}>
                        {getFieldDecorator('professionId', {
                            initialValue: educationInfo !== null ? educationInfo.professionId.id : null,
                            rules: [{required: true, message: 'Zəhmət olmasa oxuduğunuz ixtisası seçin'}],
                        })(
                            <Select showSearch={true}
                                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                    style={{width: '100%'}}>
                                {
                                    professions.map(item => {
                                        return <Select.Option key={item.id} value={item.id}>{item.name}</Select.Option>
                                    })
                                }
                            </Select>
                        )}
                    </FormItem>

                    <FormItem label="Tarix aralığı" {...formItemLayout}>
                        {getFieldDecorator('dateRange', {
                            initialValue: educationInfo !== null ? [moment(educationInfo.startDate), moment(educationInfo.endDate)] : [],
                            rules: [{required: true, message: 'Zəhmət olmasa təhsil aldığınız tarix aralığını seçin'}],
                        })(<RangePicker format="DD.MM.YYYY" placeholder={['Başlama tarixi', 'Bitmə tarixi']}/>)}
                    </FormItem>

                    <FormItem label="Təhsil forması" {...formItemLayout}>
                        {getFieldDecorator('educationForm', {
                            initialValue: educationInfo !== null ? educationInfo.educationForm : 1,
                            rules: [{required: true, message: 'Zəhmət olmasa təhsil formasını seçin'}],
                        })(
                            <Radio.Group>
                                <Radio.Button value={1}>Əyani</Radio.Button>
                                <Radio.Button value={2}>Qiyabi</Radio.Button>
                                <Radio.Button value={3}>Distant</Radio.Button>
                            </Radio.Group>
                        )}
                    </FormItem>

                </Form>

            </Modal>
        );
    }
}

const EducationForm = Form.create()(EduForm);

export default EducationForm;