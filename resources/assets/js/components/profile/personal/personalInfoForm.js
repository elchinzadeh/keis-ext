import React, {Component} from 'react';
import {Button, DatePicker, Input, InputNumber, Form, Col, Row, Radio, Select, Divider} from 'antd';
import moment from 'moment';

const FormItem = Form.Item;
const SelectOption = Select.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const InputGroup = Input.Group;

class PersonalForm extends Component {

    render() {

        const {entity, posting, handleSave, form, countries, districtCities} = this.props;

        const {getFieldDecorator} = form;

        const formItemLayout = {
            labelCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 12
                },
                lg: {
                    span: 12
                }
            },
            wrapperCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 12
                },
                lg: {
                    span: 12
                }
            }
        };

        return (
            <Form layout="horizontal" onSubmit={handleSave}>
                <Divider>Şəxsi məlumatlar</Divider>
                <Row>
                    <Col md={10} lg={10} xs={24} sm={24}>
                        <FormItem label="Ş.V Seriya" {...formItemLayout}>
                            {getFieldDecorator('idCardNo', {
                                initialValue: entity ? entity.idCardNo : null,
                                rules: [{
                                    required: true,
                                    message: 'Zəhmət olmasa şəxsiyyət vəsiqənizim seriya nömrəsini qeyd edin'
                                }],
                            })(
                                <Input type="number" style={{width: '100%'}} placeholder="Ş.V. Seriya nömrəsi" addonBefore="AZE"/>
                            )}
                        </FormItem>
                        <FormItem
                            label="FİN"
                            {...formItemLayout}>
                            {getFieldDecorator('idCardFin', {
                                initialValue: entity ? entity.idCardFin : null,
                                rules: [{required: true, message: 'Zəhmət olmasa FİNinizi daxil edin'}],
                            })(
                                <Input placeholder="Könüllünün FİNi"/>
                            )}

                        </FormItem>
                        <FormItem
                            label="Ad"
                            {...formItemLayout}>
                            {getFieldDecorator('firstName', {
                                initialValue: entity ? entity.firstName : null,
                                rules: [{required: true, message: 'Zəhmət olmasa adınızı daxil edin'}],
                            })(
                                <Input placeholder="Könüllünün adı"/>
                            )}

                        </FormItem>
                        <FormItem
                            label="Soyad"
                            {...formItemLayout}>
                            {getFieldDecorator('lastName', {
                                initialValue: entity ? entity.lastName : null,
                                rules: [{required: true, message: 'Zəhmət olmasa soyadınızı daxil edin'}],
                            })(
                                <Input placeholder="Könüllünün soyadı"/>
                            )}
                        </FormItem>
                        <FormItem
                            label="Ata adı"
                            {...formItemLayout}>
                            {getFieldDecorator('patronymic', {
                                initialValue: entity ? entity.patronymic : null,
                                rules: [{required: true, message: 'Zəhmət olmasa ata adını daxil edin'}],
                            })(
                                <Input placeholder="Könüllünün ata adı"/>
                            )}
                        </FormItem>
                        <FormItem
                            label="Doğulduğu yer"
                            {...formItemLayout}>
                            {getFieldDecorator('birthPlace', {
                                initialValue: entity ? entity.birthPlace : null,
                                rules: [{required: true, message: 'Zəhmət olmasa doğum yerinizi daxil edin'}],
                            })(
                                <Input placeholder="Könüllünün doğum yeri"/>
                            )}
                        </FormItem>
                        <FormItem
                            label="Milliyəti"
                            {...formItemLayout}>
                            {getFieldDecorator('nationality', {
                                initialValue: entity ? entity.nationality : null,
                                rules: [{required: true, message: 'Zəhmət olmasa milliyətinizi daxil edin'}],
                            })(
                                <Input placeholder="Könüllünün milliyəti"/>
                            )}
                        </FormItem>
                    </Col>
                    <Col md={10} lg={10} xs={24} sm={24}>
                        <FormItem
                            label="Doğum tarixi"
                            {...formItemLayout}>
                            {getFieldDecorator('birthDate', {
                                initialValue: entity ? moment(entity.birthDate) : null,
                                rules: [{required: true, message: 'Zəhmət olmasa doğum tarixinizi seçin'}],
                            })(
                                <DatePicker style={{width: '100%'}} format="DD.MM.YYYY"
                                            placeholder="Könüllünün doğum tarixi"/>
                            )}
                        </FormItem>
                        <FormItem
                            label="Vətəndaşlığı"
                            {...formItemLayout}>
                            {getFieldDecorator('citizenshipCountryId', {
                                initialValue: entity ? entity.citizenshipCountryId ? entity.citizenshipCountryId.id : undefined : undefined,
                                rules: [{required: true, message: 'Zəhmət olmasa vətəndaşı olduğunuz ölkəni seçin'}],
                            })(
                                <Select showSearch={true}
                                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        style={{width: '100%'}}
                                        placeholder="Seçilməyib">
                                    {
                                        countries ? countries.map(item => {
                                            return <SelectOption key={item.id}
                                                                 value={item.id}>{item.name}</SelectOption>
                                        }) : null
                                    }
                                </Select>
                            )}
                        </FormItem>
                        <FormItem
                            label="Cinsi"
                            {...formItemLayout}>
                            {getFieldDecorator('sex', {
                                initialValue: entity ? entity.sex : true,
                                rules: [{required: true, message: 'Zəhmət olmasa cinsinizi seçin'}],
                            })(
                                <RadioGroup>
                                    <RadioButton value={true}>Kişi</RadioButton>
                                    <RadioButton value={false}>Qadın</RadioButton>
                                </RadioGroup>
                            )}
                        </FormItem>
                        <FormItem
                            label="Ailə vəziyyəti"
                            {...formItemLayout}>
                            {getFieldDecorator('maritalStatus', {
                                initialValue: entity ? entity.maritalStatus : undefined,
                                rules: [{required: true, message: 'Zəhmət olmasa ailə vəziyyətinizi seçin seçin'}],
                            })(
                                <Select style={{width: '100%'}}
                                        placeholder="Seçilməyib">
                                    <SelectOption value={0}>Subay</SelectOption>
                                    <SelectOption value={1}>Evli</SelectOption>
                                </Select>
                            )}
                        </FormItem>
                        <FormItem
                            label="Qeydiyyatda olduğu şəhər"
                            {...formItemLayout}>
                            {getFieldDecorator('registeredPlaceDistrictCityId', {
                                initialValue: entity ? entity.registeredPlaceDistrictCityId ? entity.registeredPlaceDistrictCityId.id : undefined : undefined,
                                rules: [{required: true, message: 'Zəhmət olmasa qeydiyyatda olduğunuz şəhəri seçin'}],
                            })(
                                <Select showSearch={true}
                                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        style={{width: '100%'}}
                                        placeholder="Seçilməyib">
                                    {
                                        districtCities ? districtCities.map(item => {
                                            return <SelectOption key={item.id}
                                                                 value={item.id}>{item.name}</SelectOption>
                                        }) : null
                                    }
                                </Select>
                            )}
                        </FormItem>
                        <FormItem
                            label="Qeydiyyat ünvanı"
                            {...formItemLayout}>
                            {getFieldDecorator('registeredPlaceAddress', {
                                initialValue: entity ? entity.registeredPlaceAddress : null,
                                rules: [{required: true, message: 'Zəhmət olmasa qeydiyyat ünvanınızı daxil edin'}],
                            })(
                                <Input placeholder="Könüllünün qeydiyyat ünvanı"/>
                            )}
                        </FormItem>
                        <FormItem
                            label="Yaşadığı şəhər"
                            {...formItemLayout}>
                            {getFieldDecorator('livingPlaceDistrictCityId', {
                                initialValue: entity ? entity.livingPlaceDistrictCityId ? entity.livingPlaceDistrictCityId.id : undefined : undefined,
                                rules: [{required: true, message: 'Zəhmət olmasa yaşadığınız şəhəri seçin'}],
                            })(
                                <Select showSearch={true}
                                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        style={{width: '100%'}}
                                        placeholder="Seçilməyib">
                                    {
                                        districtCities ? districtCities.map(item => {
                                            return <SelectOption key={item.id}
                                                                 value={item.id}>{item.name}</SelectOption>
                                        }) : null
                                    }
                                </Select>
                            )}
                        </FormItem>
                        <FormItem
                            label="Yaşayış ünvanı"
                            {...formItemLayout}>
                            {getFieldDecorator('livingPlaceAddress', {
                                initialValue: entity ? entity.livingPlaceAddress : null,
                                rules: [{required: true, message: 'Zəhmət olmasa yaşayış ünvanınızı daxil edin'}],
                            })(
                                <Input placeholder="Könüllünün yaşayış ünvanı"/>
                            )}
                        </FormItem>

                    </Col>
                </Row>
                <Divider>Əlaqə məlumatları</Divider>
                <Row>
                    <Col md={10} lg={10} xs={24} sm={24}>
                        <FormItem
                            label="Mobil telefon"
                            {...formItemLayout}>
                            {getFieldDecorator('phoneMobile', {
                                initialValue: entity ? entity.phoneMobile : null,
                                rules: [{required: true, message: 'Zəhmət olmasa mobil nömrənizi daxil edin'}],
                            })(
                                <Input
                                    addonBefore="+994"
                                    type="number"
                                    placeholder="Könüllünün mobil nömrəsi"
                                />
                            )}
                        </FormItem>
                        <FormItem
                            label="Ev telefonu"
                            {...formItemLayout}>
                            {getFieldDecorator('phoneHouse', {
                                initialValue: entity ? entity.phoneHouse : null,
                                rules: [{required: true, message: 'Zəhmət olmasa ev nömrənizi daxil edin'}],
                            })(
                                <Input
                                    addonBefore="+994"
                                    type="number"
                                    placeholder="Könüllünün ev nömrəsi"
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col md={10} lg={10} xs={24} sm={24}>
                        <FormItem
                            label="E-poçt"
                            {...formItemLayout}>
                            <InputGroup compact>
                                {getFieldDecorator('email', {
                                    initialValue: entity ? entity.email : null,
                                    rules: [{required: false, message: 'Zəhmət olmasa e-poçt ünvanınızı daxil edin'}],
                                })(
                                    <Input style={{width: '80%'}} placeholder="Könüllünün e-poçt ünvanı" disabled/>
                                )}
                                <Button style={{width: '20%'}} icon="mail"
                                        href={entity ? 'mailto:' + entity.email : null}/>
                            </InputGroup>
                        </FormItem>
                        <FormItem
                            label="Facebook ünvanı"
                            {...formItemLayout}>
                            <InputGroup compact>
                                {getFieldDecorator('facebookProfileUrl', {
                                    initialValue: entity ? entity.facebookProfileUrl : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa facebook ünvanınızı daxil edin'}],
                                })(
                                    <Input style={{width: '80%'}} placeholder="http://facebook.com/zuck"/>
                                )}
                                <Button style={{width: '20%'}} icon="link"
                                        href={entity ? entity.facebookProfileUrl : null} target="_blank"/>
                            </InputGroup>
                        </FormItem>
                    </Col>
                </Row>
                <Divider>Digər məlumatlar</Divider>
                <Row>
                    <Col md={10} lg={10} xs={24} sm={24}>
                        <FormItem label="Hərbi mükəlləfiyyət" {...formItemLayout}>
                            {getFieldDecorator('militaryAttitude', {
                                initialValue: entity ? entity.militaryAttitude : null,
                                rules: [{required: true, message: 'Hərbi mükəlləfiyyətə münasibətinizi daxil edin'}],
                            })(
                                <Input placeholder="Hərbi mükəlləfiyyətə münasibətiniz"/>
                            )}
                        </FormItem>
                        <FormItem label="Sürücülük vəsiqəsi" {...formItemLayout}>
                            {getFieldDecorator('haveDriveLicense', {
                                initialValue: entity ? entity.haveDriveLicense : false,
                                rules: [{required: false, message: 'Sürücülük vəsiqəsi'}],
                            })(
                                <RadioGroup>
                                    <RadioButton value={true}>Var</RadioButton>
                                    <RadioButton value={false}>Yoxdur</RadioButton>
                                </RadioGroup>
                            )}
                        </FormItem>
                    </Col>
                    <Col md={10} lg={10} xs={24} sm={24}>
                        <FormItem label="Fiziki məhdudiyyət" {...formItemLayout}>
                            {getFieldDecorator('physicallyLimitation', {
                                initialValue: entity ? entity.physicallyLimitation : null,
                                rules: [{required: false, message: 'Fiziki məhdudiyyətinizi daxil edin'}],
                            })(
                                <Input placeholder="Fiziki məhdudiyyət"/>
                            )}
                        </FormItem>
                    </Col>
                </Row>
                <Row>
                    <Col span={24} style={{textAlign: 'center'}}>
                        <Button type="primary" htmlType="submit" loading={posting}>
                            Dəyişiklikləri yadda saxla
                        </Button>
                    </Col>
                </Row>
            </Form>
        );

    }

}

const PersonalInfoForm = Form.create()(PersonalForm);

export default PersonalInfoForm;
