import React, {Component} from 'react';
import {Row, Col, Card, Button, Icon, List, message, Popconfirm, Badge, Divider} from 'antd';
import moment from 'moment'

import FamilyForm from './familyForm'
import FamilyApi from "../../../api/profile/familyApi";
import HelperApi from "../../../api/helperApi";

export default class FamilyInfo extends Component {

    constructor() {
        super();
        this.state = {
            fetching: false,
            modalOpen: false,
            entities: [],
            totalCount: 0,
            selectedEntity: null,
            kinshipTypes: []
        }
    }

    componentWillMount() {
        this.loadFamilyInfo();
        this.getKinshipTypes()
    }

    loadFamilyInfo = () => {

        this.setState({
            fetching: true
        });

        FamilyApi.getFamilyInfo().then(response => {
            this.setState({
                fetching: false
            });

            if (response.data.error === null) {
                this.setState({
                    entities: response.data.data.entities,
                    totalCount: response.data.data.totalCount
                })
            }else{
                message.error(response.data.error.message)
            }
        }).catch(
            this.setState({
                fetching: false
            })
        )
    };

    showModal = () => {
        this.setState({
            modalOpen: true,
            selectedEntity: null
        })
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    handleCreate = () => {

        const form = this.formRef.props.form;

        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            if (this.state.selectedEntity === null) {
                this.insertFamilyInfo(values);
            } else {
                this.updateFamilyInfo(values);
            }
        });
    };

    handleCancel = () => {
        this.setState({
            modalOpen: false
        })
    };

    editFamilyInfo = (item) => {
        this.setState({
            modalOpen: true,
            selectedEntity: item
        });
    };

    insertFamilyInfo = (values) => {

        const form = this.formRef.props.form;
        
        const body = {
            nameSurname: values.nameSurname,
            kinshipTypeId: {id: values.kinshipTypeId},
            birthDate: values.birthDate.valueOf(),
            birthPlace: values.birthPlace,
            jobInformation: values.jobInformation
        };

        FamilyApi.insertFamilyInfo(body).then(response => {
            if (response.data.error === null) {
                this.setState({modalOpen: false});
                this.loadFamilyInfo();
                form.resetFields();
            } else {
                message.error(response.data.error.message);
            }
        }).catch();
    };

    updateFamilyInfo = (values) => {
        const form = this.formRef.props.form;
        const body = {
            id: this.state.selectedEntity.id,
            nameSurname: values.nameSurname,
            kinshipTypeId: {id: values.kinshipTypeId},
            birthDate: values.birthDate.valueOf(),
            birthPlace: values.birthPlace,
            jobInformation: values.jobInformation,
        };

        FamilyApi.updateFamilyInfo(body).then(response => {
            if (response.data.error === null) {
                this.setState({modalOpen: false});
                this.loadFamilyInfo();
                form.resetFields();
            } else {
                message.error(response.data.error.message);
            }
        }).catch();
    };

    deleteFamilyInfo = (id) => {
        FamilyApi.deleteFamilyInfo(id).then(response => {

            if (response.data.error === null) {
                this.loadFamilyInfo();
            } else {
                message.error(response.data.error.message);
            }

        }).catch();
    };

    getKinshipTypes = () => {
        HelperApi.getKinshipTypes().then(response => {
            if (response.data.error === null) {
                this.setState({
                    kinshipTypes: response.data.data.entities
                })
            }
        }).catch()
    };

    render() {

        return (
            <Card loading={this.state.fetching}>

                <Divider>Ailə məlumatları</Divider>

                <FamilyForm
                    wrappedComponentRef={this.saveFormRef}
                    familyInfo={this.state.selectedEntity}
                    kinshipTypes={this.state.kinshipTypes}
                    visible={this.state.modalOpen}
                    onCreate={this.handleCreate}
                    onCancel={this.handleCancel}
                />

                <Row>
                    <Col md={{span: 14, push: 5}} lg={{span: 14, push: 5}} xs={24} sm={24} style={{textAlign: 'right'}}>
                        <Button type="primary" onClick={this.showModal}>
                            <Icon type="plus-circle-o"/>Əlavə et
                        </Button>
                    </Col>
                </Row>

                <Row>
                    <Col md={{span: 14, push: 5}} lg={{span: 14, push: 5}} xs={24} sm={24}>
                        <List
                            itemLayout="horizontal"
                            dataSource={this.state.entities}
                            renderItem={item => (
                                <List.Item actions={[

                                    <a onClick={() => this.editFamilyInfo(item)}>Dəyişiklik et</a>,
                                    <Popconfirm title="Təhil məlumatını silməyə əminsinizmi?"
                                                onConfirm={() => this.deleteFamilyInfo(item.id)}
                                                okText="Bəli" cancelText="Xeyr">
                                        <a style={{color: 'red'}}>Sil</a>
                                    </Popconfirm>
                                ]}>
                                    <List.Item.Meta
                                        title={<a onClick={(e) => { e.preventDefault(); }}>{item.nameSurname} <Badge count={item.kinshipTypeId.name} style={{backgroundColor: '#52c41a'}}/></a>}
                                        description={item.jobInformation}
                                    />
                                    <div>
                                        <h4>{item.birthPlace}</h4>
                                        {moment(item.birthDate).format('LL')}
                                    </div>
                                    <div>{}</div>
                                </List.Item>
                            )}
                        />
                    </Col>
                </Row>
            </Card>
        );
    }

}

