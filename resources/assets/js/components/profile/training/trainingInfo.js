import React, {Component} from 'react';
import {Col, Row, Card, List, Button, Icon, Popconfirm, message, Divider} from 'antd'
import moment from 'moment';

import TrainingForm from './trainingForm'

import TrainingApi from "../../../api/profile/trainingApi";
import FileUploadApi from "../../../api/fileUploadApi";

export default class TrainingInfo extends Component {

    constructor() {
        super();
        this.state = {
            fetching: false,
            modalOpen: false,
            entities: [],
            selectedEntity: null,
            totalCount: 0,
            filePath: null
        }
    }

    componentDidMount() {
        this.loadTrainingInfo();
    }

    loadTrainingInfo = () => {
        this.setState({
            fetching: true
        });

        TrainingApi.getTrainingInfo().then(response => {
            this.setState({
                fetching: false
            });

            if (response.data.error == null) {
                this.setState({
                    entities: response.data.data.entities,
                    totalCount: response.data.data.totalCount
                });
            }else{
                message.error(response.data.error)
            }
        }).catch(error => {
            this.setState({
                fetching: false
            });

            message.error(error)
        });
    };

    insertTrainingInfo = (values) => {
        const form = this.formRef.props.form;

        const body = {
            trainingName: values.trainingName,
            trainingDate: values.trainingDate,
        };

        TrainingApi.insertTrainingInfo(body).then(response => {

            if (response.data.error === null) {
                message.success('Məlumat bazaya əlavə olundu');
                this.loadTrainingInfo();
                form.resetFields();
                this.setState({modalOpen: false});
            } else {
                message.error(response.data.error.message);
            }

        }).catch();
    };

    editTrainingInfo = (item) => {
        this.setState({
            selectedEntity: item,
            modalOpen: true
        });
    };

    updateTrainingInfo = (values) => {
        const form = this.formRef.props.form;

        const body = {
            id: this.state.selectedEntity.id,
            trainingName: values.trainingName,
            trainingDate: values.trainingDate,
        };

        TrainingApi.updateTrainingInfo(body).then(response => {

            if (response.data.error === null) {
                message.success('Məlumat yeniləndi');
                this.loadTrainingInfo();
                form.resetFields();
                this.setState({modalOpen: false});
            } else {
                message.error(response.data.error.message);
            }
        }).catch();
    };

    deleteTrainingInfo = (id) => {
        TrainingApi.deleteTrainingInfo(id).then(response => {

            if (response.data.error === null) {
                message.success('Məlumat bazadan silindi');
                this.loadTrainingInfo();
            } else {
                message.error(response.data.error.message);
            }

        }).catch();
    };

    showModal = () => {
        this.setState({
            modalOpen: true,
            selectedEntity: null
        });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    handleCancel = () => {
        this.setState({
            modalOpen: false
        })
    };

    handleCreate = (file) => {
        // this.uploadFile(file);
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            // if (err) {
            //     return
            // }
            // if (this.state.filePath) {
            if (!err){
                if (this.state.selectedEntity === null) {
                    this.insertTrainingInfo(values);
                } else {
                    this.updateTrainingInfo(values);
                }
            }
            // }
        });
    };

    uploadFile = (file) => {
        const body = {
            tableName: 'UserTraining',
            dataType : 'image',
            data: file
        };

        FileUploadApi.upload(body).then(response => {
            console.log(response)
        }).catch(error => {
            console.log(error)
        })
    };

    render() {
        return (
            <Card loading={this.state.fetching}>
                <Divider>Təlim məlumatları</Divider>
                <TrainingForm wrappedComponentRef={this.saveFormRef}
                                visible={this.state.modalOpen}
                                trainingInfo={this.state.selectedEntity}
                                areas={this.state.areas}
                                onCancel={this.handleCancel}
                                onCreate={this.handleCreate}
                />

                <Row>
                    <Col xs={24} sm={24} md={{span: 14, offset: 5}} lg={{span: 14, offset: 5}} style={{textAlign: 'right'}}>
                        <Button type="primary" onClick={this.showModal}>
                            <Icon type="plus-circle-o"/>Əlavə et
                        </Button>
                    </Col>
                </Row>

                <Row>
                    <Col xs={24} sm={24} md={{span: 14, offset: 5}} lg={{span: 14, offset: 5}}>

                        <List   dataSource={this.state.entities}
                                renderItem={item => (
                                    <List.Item actions={[
                                        <a onClick={() => this.editTrainingInfo(item)}>Dəyişiklik et</a>,
                                        <Popconfirm title="Təlim məlumatını silməyə əminsinizmi?"
                                                    onConfirm={() => this.deleteTrainingInfo(item.id)}
                                                    okText="Bəli"
                                                    cancelText="Xeyr">
                                            <a style={{color: 'red'}}>Sil</a>
                                        </Popconfirm>
                                    ]}>

                                        <List.Item.Meta
                                            title={<a onClick={(e) => { e.preventDefault(); }}>{item.trainingName}</a>}
                                        />
                                        <div>
                                            {moment(item.trainingDate).format('LL')}

                                        </div>
                                    </List.Item>

                                )}
                        />

                    </Col>
                </Row>
            </Card>
        )
    }
}