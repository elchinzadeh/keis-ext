import React, {Component} from 'react';
import {Modal, Form, DatePicker, Input, Upload, message, Button, Icon} from 'antd';
import moment from 'moment';

const FormItem = Form.Item;

class TrainForm extends Component {
    state = {
        fileList: [],
        file: null,
        uploading: false,
    };

    handleCreate = () => {
        if (this.state.fileList[0]) {
            this.convertBase64(this.state.fileList[0]);
        }
    };

    convertBase64 = (file) => {
        let base64file = null;
        if (/\.(jpe?g|png)$/i.test(file.name)) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                //
            };
            reader.onloadend = () => {
                base64file = reader.result;
                this.props.onCreate(base64file)
            };
            reader.onerror = function (error) {
                message.error(error);
            };
        }else{
            message.error('Zəhmət olmasa .jpg və ya .png uzantılı fayl seçin')
        }
    };

    render() {
        const {visible, onCancel, onCreate, form, trainingInfo} = this.props;

        const {getFieldDecorator} = form;

        const formItemLayout = {
            labelCol: {
                sm: {span: 24},
                xs: {span: 24},
                md: {span: 6},
                lg: {span: 6}
            },
            wrapperCol: {
                sm: {span: 24},
                xs: {span: 24},
                md: {span: 18},
                lg: {span: 18}
            }
        };

        const uploadProps = {
            onRemove: (file) => {
                this.setState(({ fileList }) => {
                    const index = fileList.indexOf(file);
                    const newFileList = fileList.slice();
                    newFileList.splice(index, 1);
                    return {
                        fileList: newFileList,
                    };
                });
            },
            beforeUpload: (file) => {
                this.setState({
                    fileList: [file],
                });
                return false;
            },
            fileList: this.state.fileList,
        };

        return (
            <Modal
                visible={visible}
                title={trainingInfo === null ? 'Təlim məlumatının əlavə edilməsi' : 'Təlim məlumatının redaktəsi'}
                okText="Təsdiq"
                cancelText="İmtina"
                onCancel={onCancel}
                onOk={onCreate}>

                <Form layout="horizontal">

                    <FormItem label="Adı" {...formItemLayout}>
                        {getFieldDecorator('trainingName', {
                            initialValue: trainingInfo !== null ? trainingInfo.trainingName : null,
                            rules: [{ required: true, message: 'Zəhmət olmasa vəzifənizi daxil edin' }],
                        })(
                            <Input placeholder="Adı" />
                        )}
                    </FormItem>

                    <FormItem label="Tarix" {...formItemLayout}>
                        {getFieldDecorator('trainingDate', {
                            initialValue: trainingInfo !== null ? moment(trainingInfo.trainingDate) : null,
                            rules: [{required: true, message: 'Zəhmət olmasa tarixi seçin'}],
                        })(
                            <DatePicker format="DD.MM.YYYY" placeholder="Tarix" style={{width: '100%'}}/>
                        )}
                    </FormItem>

                    {/*<FormItem label="Sertifikat" {...formItemLayout}>*/}
                        {/*<Upload {...uploadProps}>*/}
                            {/*<Button>*/}
                                {/*<Icon type="upload" /> Faylı seç*/}
                            {/*</Button>*/}
                        {/*</Upload>*/}
                    {/*</FormItem>*/}
                </Form>

            </Modal>
        );
    }
}

const TrainingForm = Form.create()(TrainForm);

export default TrainingForm;