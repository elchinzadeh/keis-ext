import React, { Component } from 'react';
import {Button, message, Upload} from 'antd'

import './profileImage.css';
import ProfileApi from "../../../api/profile/profileApi";

export default class ProfileImage extends Component {

    constructor() {
        super();

        this.state = {
            image: 'https://outthere.podbean.com/mf/web/qmbakx/avatar.jpg',
            showModal: false,
            fileList: []
        }
    }

    componentDidMount() {
        this.getProfileImage()
    }

    getProfileImage = () => {
        ProfileApi.getPersonalInfo().then(response => {
            if (response.data.error === null) {
                if (response.data.data.image !== null) {
                    this.setState({
                        image: response.data.data.image
                    })
                }
            }
        })
    };

    convertBase64 = (file) => {
        let base64file = null;
        if (/\.(jpe?g|png)$/i.test(file.name)) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = () => {
                base64file = reader.result;
                this.updateImage(base64file)
            };
            reader.onerror = function (error) {
                message.error(error);
            };
        }else{
            message.error('Zəhmət olmasa .jpg və ya .png uzantılı fayl seçin')
        }
    };

    updateImage = (file) => {
        const body = {
            image: file
        };
        ProfileApi.updateImage(body).then(response => {
            if (response.data.error === null){
                this.getProfileImage();
                message.success('Profil şəkli yeniləndi')
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    render() {
        const uploadProps = {
            beforeUpload: (file) => {
                this.setState({
                    fileList: [file],
                });
                this.convertBase64(file);
                return false;
            },
            showUploadList: false
        };

        return(
            <div className="profile-image">
                <div className="image" style={{backgroundImage: 'url("'+this.state.image+'")' }} />

                <div className="edit-btn">
                    <Upload {...uploadProps}>
                        <Button type="normal" shape="circle" icon="edit" size={'small'} onClick={this.showModal} />
                    </Upload>
                </div>

            </div>
        )
    }
}

//https://netbranding.co.nz/wp-content/uploads/2014/05/avatar-1.png

