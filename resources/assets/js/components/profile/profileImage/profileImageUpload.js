import React from 'react';
import {Button, Modal} from 'antd';

export default class ProfileImageUpload extends React.Component {

    constructor() {
        super();

        this.state = {
            //
        }
    }

    componentDidMount() {
        //
    }

    render() {
        const {visible, handleOk, handleCancel} = this.props;

        return(
            <Modal
                title=""
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <Button>Click</Button>
            </Modal>
        )
    }
}