import React from 'react';
import {Layout} from 'antd';

const {Footer} = Layout;

const AppFooter = (props) => {

    return (
        <Footer className={'footer'} style={{textAlign: 'center'}}>
            Könüllülük Prosesinin Elektron İdarəetmə Sistemi ©2018 ASAN Xidmət
        </Footer>
    );

};

export default AppFooter;