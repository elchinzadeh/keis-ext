import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import {Layout, Icon, Popover, Divider} from 'antd';
import AuthApi from "../../api/authApi";
import cookie from 'react-cookies'
const {Header} = Layout;

class AppHeader extends Component {

    logout = () => {
        cookie.remove('ut');
        cookie.remove('itl');
        AuthApi.logout();
        window.location.replace('/login')
    };

    render() {

        const content = (
            <div className="topnav-mobile">
                <NavLink className="nav-link-mobile" to="/dashboard" activeClassName="active" exact>
                    Lövhə
                </NavLink>
                <NavLink className="nav-link-mobile" to="/profile" activeClassName="active">
                    Profil
                </NavLink>
                <NavLink className="nav-link-mobile" to="/attendance">
                    Davamiyyət
                </NavLink>
                <Divider/>
                <NavLink className="nav-link-mobile" to="/kadr">
                    ASAN Kadr
                </NavLink>
            </div>
        );
        const isTeamLeader = cookie.load('itl');
        return (
            <Header style={{background: '#fff'}}>

                <NavLink to="/">
                    <img className="logo" src="../../img/logo.png" alt="..."/>
                </NavLink>

                <div className="topnav">

                    <NavLink className="nav-link" to="/dashboard" activeClassName="active" exact>
                        Lövhə
                    </NavLink>
                    <NavLink className="nav-link" to="/profile" activeClassName="active">
                        Profil
                    </NavLink>
                    <NavLink className="nav-link" to="/attendance">
                        Davamiyyət
                    </NavLink>
                    {
                        isTeamLeader == 'true' ?
                            <NavLink className="nav-link" to="/teamLeaderPanel">
                                Qrup rəhbəri paneli
                            </NavLink> : null
                    }
                    <NavLink className="nav-link external-link" to="#" onClick={() => this.logout()}>
                        Çıxış
                    </NavLink>
                    <Popover placement="bottom" content={content} trigger="click">
                        <a className="nav-collapse">
                            <Icon type="bars" style={{ fontSize: 20 }}/>
                        </a>
                    </Popover>
                </div>

            </Header>
        );

    }

}

export default AppHeader;