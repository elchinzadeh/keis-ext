import React from 'react'
import {Modal, Avatar, Row, Col, Rate, Form, Input} from 'antd'
import GroupLeaderApi from '../../api/groupLeaderApi'

export default class AssessmentModal extends React.Component{

    constructor() {
        super();

        this.state = {
            user: null,
            criterias: null,
            results: {
                note: null,
                marks: []
            }
        }
    };

    componentDidMount() {
        this.getSingleAssessment(this.props.selectedEntityId)
    }

    getSingleAssessment = (id) => {
        GroupLeaderApi.getSingleAssessment(id).then(response => {
            if (response.data.error === null){
                this.setState({
                    user: response.data.data.user,
                    criterias: response.data.data.criterias,

                })
            }
        })
    };

    handleChange = (id, num) => {
        let arr = this.state.results.marks,
            status = false;

        for (let i = 0; i < arr.length; i++) {
            if (arr[i].id === id) {
                arr[i].num = num;
                status = true;
                break
            }
        }

        if (status === false) {
            arr.push({
                id: id,
                num: num
            })
        }

        this.setState({
            results: {
                ...this.state.results,
                marks: arr
            }
        });
    };

    noteOnChange = (e) => {
        this.setState({
            results: {
                ...this.state.results,
                note: e.target.value
            }
        })
    };

    render() {
        const {visible, handleOk, handleCancel} = this.props;
        const {user, criterias} = this.state;

        return (
            <Modal
                title="Qiymətləndirmə"
                visible={visible}
                onOk={() => handleOk(this.state.results)}
                onCancel={handleCancel}
                width={700}
            >
                {
                    user ?
                        <h2>
                            <Avatar shape="square" size="large" src={user.image} style={{marginRight: 16}} />
                            {user.firstName + ' ' + user.lastName + ' ' + user.patronymic}
                        </h2> : null
                }
                <br/>
                {
                    criterias ?
                        <React.Fragment>
                            {
                                criterias.map(item =>
                                    <Row key={item.id}>
                                        <Col md={16}>
                                            <h3>{item.name}</h3>
                                        </Col>
                                        <Col md={8}>
                                            <Rate
                                                onChange={value => this.handleChange(item.id, value)}
                                            />
                                        </Col>
                                    </Row>
                                )
                            }
                        </React.Fragment> : null

                }
                <br/>
                <h4>Qeyd</h4>
                <Input.TextArea rows={3} onChange={(e) => this.noteOnChange(e)}/>
            </Modal>
        )
    }
}