import React from 'react'
import { List, Avatar, Skeleton, Divider, message } from 'antd'
import AssessmentModal from './assessmentModal';
import GroupLeaderApi from '../../api/groupLeaderApi'

export default class SecondAssessment extends React.Component {
    constructor() {
        super();

        this.state = {
            data: [],
            loading: false,
            modalOpen: false,
            selectedEntityId: null
        }
    }

    componentDidMount() {
        this.getSecondAssessment()
    }

    getSecondAssessment = () => {
        this.setState({loading: true});
        GroupLeaderApi.getSecondAssessment().then(response => {
            if (response.data.error === null){
                this.setState({
                    data: response.data.data,
                    loading: false
                })
            }else{
                message.error(response.data.error.message);
                this.setState({loading: false});
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    showModal = (id) => {
        this.setState({
            modalOpen: true,
            selectedEntityId: id
        })
    };

    closeModal = () => {
        this.setState({
            modalOpen: false
        })
    };

    handleSubmit = (body) => {
        body = {
            ...body,
            userId: this.state.selectedEntityId
        };

        GroupLeaderApi.submitSecondAssesment(body).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat bazaya əlavə olundu');
                this.setState({
                    modalOpen: false
                });
                this.getSecondAssessment()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    render() {
        const {loading} = this.state;

        return(
            <React.Fragment>
                {
                    this.state.data.map(group =>
                        <React.Fragment key={group.groupName}>
                            <Divider>{group.groupName}</Divider>
                            <List
                                loading={loading}
                                itemLayout="horizontal"
                                dataSource={group.userList}
                                renderItem={user => (
                                    <List.Item actions={[<a onClick={() => this.showModal(user.id)}>Qiymətləndir</a>]}>
                                        <Skeleton avatar loading={loading} active>
                                            <List.Item.Meta
                                                avatar={<Avatar src={user.image} />}
                                                title={user.firstName + ' ' + user.lastName + ' ' + user.patronymic}
                                            />
                                        </Skeleton>
                                    </List.Item>
                                )}
                            />
                        </React.Fragment>
                    )
                }
                {
                    this.state.modalOpen ?
                        <AssessmentModal
                            visible={this.state.modalOpen}
                            selectedEntityId={this.state.selectedEntityId}
                            handleCancel={this.closeModal}
                            handleOk={this.handleSubmit}
                        /> : null
                }
            </React.Fragment>
        )
    }
}