import React from 'react';
import { Col, Row, Icon, Button, Progress } from 'antd';

import { DashboardContext } from "../../../contexts/dashboardContext";

const ExamResult = (props) => {
    const { countOfCorrectAnswer, countOfQuestion, percentage, succeed } = props.testResult;

    return(
        <DashboardContext.Consumer>
            {value =>
                <Col align="center">
                    <Row className="">
                        <h1><Icon type={succeed ? 'smile-o' : 'frown-o'}/></h1>
                        <h2>{succeed ? 'Siz imtahandan uğurla keçdiniz' : 'Siz imtahanda uğursuz oldunuz'}</h2>

                    </Row>
                    <Row className="mb-30">
                        <Progress type="circle" percent={percentage} width={100}
                                  status={succeed ? 'success' : 'exception'} format={percentage => `${percentage}%`}/>
                    </Row>
                    <Row className="">
                        <h4>Siz, {countOfQuestion} sualdan {countOfCorrectAnswer} suala
                            doğru, {countOfQuestion - countOfCorrectAnswer} suala yalış cavab verdiniz</h4>
                        <Button type="primary" onClick={() => value.getStatus()}>Davam et</Button>
                    </Row>
                </Col>
            }
        </DashboardContext.Consumer>
    )
};

export default ExamResult;