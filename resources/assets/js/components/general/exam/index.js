import React, {Component} from 'react';
import {Card, message} from 'antd';

import TestApi from "../../../api/general/testApi";
import TestInfo from "./testInfo";
import Test from "./test";
import ExamResult from "./examResult";


export default class Exam extends Component {

    constructor() {
        super();

        this.state = {
            fetching: true,
            duration: 0,
            timeLeft: 0,
            totalCount: 0,
            testQuestions: [],
            currentTest: 0,
            testAnswers: [],
            testResult: [],
            showResult: false,
            warningNotification: true
        }
    }

    componentDidMount() {
        this.getTestInfo();
    }

    getTestInfo = () => {
        TestApi.getTest().then(response => {
            if (response.data.error === null) {
                this.setState({
                    fetching: false,
                    duration: response.data.data.duration,
                    timeLeft: response.data.data.timeLeft,
                    // timeLeft: 4542,
                    totalCount: response.data.data.totalCount,
                    testQuestions: response.data.data.testQuestions
                });
                this.countdown();
            } else {
                message.error(response.data.error.message)
            }
        }).catch(err => {
        });
    };

    countdown = () => {
        const interval = setInterval(() => {
            if (this.state.timeLeft >= 1000) {
                this.setState({
                    timeLeft: this.state.timeLeft - 1000
                })
            }else{
                message.error('İmtahan vaxtınız bitdi!', 5);
                clearInterval(interval);
            }

            if (this.state.timeLeft <= 30000) {
                message.warn('İmtahan vaxtınız bitir. İmtahanı bitirməyiniz xahiş olunur. Əks halda imtahan uğursuz nəticələnəcək!', this.state.timeLeft / 1000 - 1)
            }
        }, 1000)
    };

    getTest = (value) => {
        this.setState({
            currentTest: value
        })
    };

    onVariantChange = (e) => {
        let {testQuestions, currentTest} = this.state,
            radioValue = e.target.value - 1;

        testQuestions[this.state.currentTest].answer = e.target.value;

        this.setQuestionAnswer(testQuestions[currentTest].testQuestion.id, testQuestions[currentTest].listTestQuestionVariant[radioValue].id);

        this.setState({
            testQuestions: testQuestions,
        })
    };

    setQuestionAnswer = (questionId, answerId) => {
        let arr = this.state.testAnswers,
            status = false;

        for (let i = 0; i < arr.length; i++) {
            if (arr[i].questionId === questionId) {
                arr[i].answerId = answerId;
                status = true;
                break
            }
        }

        if (status === false) {
            arr.push({
                questionId: questionId,
                answerId: answerId
            })
        }

        this.setState({
            testAnswers: arr
        });
    };

    finishExam = () => {
        let body = this.state.testAnswers;

        if (body.length === this.state.totalCount) {
            TestApi.submitTest(body).then(response => {
                if (response.data.error === null) {
                    this.setState({
                        testResult: response.data.data,
                        showResult: true
                    })
                } else {
                    message.error(response.data.error.message)
                }
            });
        } else {
            message.config({
                duration: 5,
                maxCount: 3,
            });
            message.error('Bütün suallar cavablanmalıdır')
        }
    };

    next = () => {
        this.setState({
            currentTest: this.state.currentTest + 1
        })
    };

    prev = () => {
        this.setState({
            currentTest: this.state.currentTest - 1
        })
    };

    render() {
        const {duration, timeLeft, totalCount, testQuestions, currentTest} = this.state;

        return (
            <Card bordered={false} loading={this.state.fetching}>
                {!this.state.showResult ?
                    (<div>
                        <TestInfo duration={duration}
                                  timeLeft={timeLeft}
                                  currentTest={currentTest}
                                  totalCount={totalCount}
                                  testQuestions={testQuestions}
                                  getTest={this.getTest}
                                  finishExam={this.finishExam}
                        />
                        <Test testQuestions={testQuestions}
                              totalCount={totalCount}
                              currentTest={currentTest}
                              onVariantChange={this.onVariantChange}
                              prev={this.prev}
                              next={this.next}
                        />
                    </div>) : <ExamResult testResult={this.state.testResult}/>
                }
            </Card>
        )
    }
}