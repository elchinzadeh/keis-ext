import React from 'react';
import {Col, Row, Button, Icon, Radio} from 'antd';

const RadioGroup = Radio.Group;

const Test = (props) => {
    const {testQuestions, currentTest, totalCount, onVariantChange, prev, next} = props;
    const radioStyle = {
        display: 'block',
        height: '30px',
        lineHeight: '30px',
    };

    return(
        <Col lg={18} sm={24}>
            <Row>
                <h2>{testQuestions[currentTest] ? (currentTest + 1 + '. ') + testQuestions[currentTest].testQuestion.question : ''}</h2>
            </Row>
            <Row className="mb-30">
                <Col>
                    <RadioGroup onChange={onVariantChange} value={testQuestions[currentTest].answer}>
                        {testQuestions[currentTest] ? testQuestions[currentTest].listTestQuestionVariant.map((item, index) => {
                            return <Radio style={radioStyle} value={index + 1} key={item.id}>{item.answer}</Radio>
                        }) : ''}
                    </RadioGroup>
                </Col>
            </Row>
            <Row className="align-center">
                <Button.Group>
                    <Button type="primary" disabled={!(currentTest > 0)} onClick={prev}>
                        <Icon type="left" />Əvvəlki
                    </Button>
                    <Button type="primary" disabled={currentTest === (totalCount - 1)}  onClick={next}>
                        Sonrakı<Icon type="right" />
                    </Button>
                </Button.Group>
            </Row>
        </Col>
    )
};

export default Test;