import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import AuthApi from "../../../api/authApi";

export default class CheckToken extends React.Component{
    constructor() {
        super();
        this.state = {
            isLoggedIn: null
        };
        AuthApi.checkToken().then(response => {
            if(!response.data.error){
                if(response.data.data.isActive){
                    this.setState({isLoggedIn: true})
                }else {
                    this.setState({isLoggedIn: false})
                }
            }else {
                this.setState({isLoggedIn: false})
            }
        }).catch(error => {
                document.writeln(error)
            }
        )
    }
    render () {
        const authUrl = ['/login', '/registration'];

        return (
            <React.Fragment>
                {this.state.isLoggedIn !== null
                    ? this.state.isLoggedIn === true
                        ? !authUrl.includes(this.props.path)
                            ? <Route path={this.props.path} component={this.props.component}/>
                            : <Redirect to="/dashboard"/>
                        : authUrl.includes(this.props.path)
                            ? <Route path={this.props.path} component={this.props.component}/>
                            : <Redirect to="/login"/>
                    : null
                }
            </React.Fragment>
        )
    }
}