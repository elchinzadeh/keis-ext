import React from 'react';
import {Card, Table} from 'antd';
import moment from 'moment';
import AttendanceApi from "../../api/attendanceApi";

const { Column } = Table;

export default class Attendance extends React.Component {

    constructor() {
        super();
        this.state = {
            entities: null
        }
    }

    componentDidMount() {
        this.getAttendanceInfo();
    }

    getAttendanceInfo = () => {
        AttendanceApi.get().then(response => {

            if (response.data.error === null) {
                const entities = response.data.data.entities.map((entity) => {
                    return {
                        date: moment(entity.date).format('LL'),
                        entryTime: moment(entity.entryTime, 'HH:mm:ss').format('HH:mm'),
                        exitTime: moment(entity.exitTime, 'HH:mm:ss').format('HH:mm')
                    }
                });
                this.setState({
                    entities: entities
                });
            }
        })
    };

    render() {
        return (
            <Card bordered={false} align="center">
                <h2>Davamiyyət</h2>

                    <Table  dataSource={this.state.entities}
                            locale={{emptyText: 'Məlumat tapılmadı :( Deyəsən nəsə qaydasında deyil'}}
                            pagination={false}>
                    <Column
                        title="Tarix"
                        dataIndex="date"
                        key="date"
                    />
                    <Column
                        title="Giriş saatı"
                        dataIndex="entryTime"
                        key="entryTime"
                    />
                    <Column
                        title="Çıxış saatı"
                        dataIndex="exitTime"
                        key="exitTime"
                    />
                </Table>
            </Card>
        )
    }
}