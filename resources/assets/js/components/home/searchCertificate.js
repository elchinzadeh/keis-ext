import React, {Component} from 'react';
import { Form, Icon, Input, Button } from 'antd';

const FormItem = Form.Item;

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class HorizontalLoginForm extends Component {

    constructor() {
        super();
        // init state
        this.state = {
            searching: false
        };
    }

    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };

    getCertificate = () => {
        this.setState({
            searching: true
        });
        setTimeout(() => {
            this.setState({
                searching: false
            });
        }, 2000);
    };

    render() {
        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

        // Only show error after a field is touched.
        const userNameError = isFieldTouched('certificateNumber') && getFieldError('certificateNumber');

        return (
            <Form layout="inline" onSubmit={this.handleSubmit}>
                <FormItem validateStatus={userNameError ? 'error' : ''} help={userNameError || ''}>
                    {getFieldDecorator('certificateNumber', {
                        rules: [{ required: false, message: 'Sertifikat nömrəsini daxil edin' }],
                    })(
                        <Input prefix={<Icon type="solution" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Sertifikat nömrəsi" />
                    )}
                </FormItem>
                <FormItem>
                    <Button type="primary" onClick={this.getCertificate} loading={this.state.searching} htmlType="submit" disabled={hasErrors(getFieldsError())}>
                        { this.state.searching ? 'Yoxlanılır' : 'Yoxla' }
                    </Button>
                </FormItem>
            </Form>
        );
    }
}

const SearchCertificate = Form.create()(HorizontalLoginForm);

export default SearchCertificate