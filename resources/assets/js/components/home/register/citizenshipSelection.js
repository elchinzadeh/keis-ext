import React from 'react';
import {Col, Row, Card, Button, Divider, Layout} from 'antd';

import azmap from '../../../img/azerbaijan_map.svg';
import worldmap from '../../../img/earth_map.svg'

export default class CitizenshipSelection extends React.Component {

    render() {
        const   iconStyle = {
                    width: '135px',
                },
                cardStyle = {
                    textAlign: 'center',
                    margin: '16px'
                };

        return (
            <React.Fragment>
                <Row>
                    <Divider>
                        <h2 className="title">Vətəndaşlığınızı seçin</h2>
                    </Divider>
                </Row>
                <Row type="flex" justify="center" >
                    <Col sm={24} md={16} lg={14} xl={10} xxl={82} >
                        <Row>
                            <Col sm={24} md={12}>
                                <Card bordered={false}
                                      style={cardStyle}
                                      hoverable
                                      onClick={() => this.props.setCitizenship(0)}
                                >
                                    <img src={azmap} alt="" style={iconStyle} />
                                    <h3>Azərbaycan vətəndaşı</h3>
                                </Card>
                            </Col>
                            <Col sm={24} md={12}>
                                <Card bordered={false}
                                      style={cardStyle}
                                      hoverable
                                      onClick={() => this.props.setCitizenship(1)}
                                >
                                    <img src={worldmap} alt="" style={iconStyle} />
                                    <h3>Əcnəbi vətəndaş</h3>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row type="flex" justify="center" >
                    <p>Hesabınız var? <a href="/login">Daxil olun</a></p>
                </Row>
            </React.Fragment>
        )
    }
};