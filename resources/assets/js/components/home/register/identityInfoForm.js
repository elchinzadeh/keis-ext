import React from 'react'
import {Col, Row, Form, Icon, Input, Divider, Button, message} from 'antd';
import Recaptcha from 'react-recaptcha';

const FormItem = Form.Item;

class IdForm extends React.Component {
    state = {
        verified: false
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err){
                if (this.state.verified) {
                    this.props.setIdentityInfo(values)
                }else {
                    message.warn('Zəhmət olmasa robot olmadığınızı təsdiqləyin')
                }
            }
        });
    };

    callback = () => {
        this.setState({verified: false})
    };

    verify = (a) => {
        if (a) {
            this.setState({verified: true})
        }
    };

    expired = () => {
        this.setState({verified: false})
    };

    render() {
        const { prevStep } = this.props;
        const { getFieldDecorator } = this.props.form;
        return(
            <React.Fragment>
                <script
                    src="https://www.google.com/recaptcha/api.js?render=explicit"
                    async
                    defer
                />
                <Row>
                    <Divider>
                        <h2 className="title">Şəxsiyyət vəsiqəsi</h2>
                    </Divider>
                </Row>
                <Row>
                    <Col md={{span: 6, offset: 9}}>
                        <Form onSubmit={this.handleSubmit} className="login-form">
                            <FormItem>
                                {getFieldDecorator('idCardNo', {
                                    initialValue: this.props.idCardNo ? this.props.idCardNo : null,
                                    rules: [{ required: true, message: 'Zəhmət olmasa şəxsiyyət vəsiqəsinin seriya nömrəsini daxil edin' }],
                                })(
                                    <Input prefix={<Icon type="idcard" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Ş.V. seriya nömrəsi" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('idCardFin', {
                                    initialValue: this.props.idCardFin ? this.props.idCardFin : null,
                                    rules: [{ required: true, message: 'Zəhmət olmasa şəxsiyyət vəsiqəsinin FİN kodunu daxil edin' }],
                                })(
                                    <Input prefix={<Icon type="idcard" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Ş.V. FİN kodu" />
                                )}
                            </FormItem>
                            <FormItem>
                                <Recaptcha
                                    sitekey="6LdrRHYUAAAAAPrko58VrVRvhTczN-jCoYS-Nooe"
                                    render="explicit"
                                    onloadCallback={this.callback}
                                    verifyCallback={this.verify}
                                    expiredCallback={this.expired}
                                    hl="az"
                                />
                            </FormItem>
                        </Form>
                    </Col>
                </Row>
                <Row type="flex" justify="center">
                    <Button.Group>
                        <Button onClick={() => prevStep()}>
                            <Icon type="left" />Geri
                        </Button>
                        <Button type="primary" onClick={e => this.handleSubmit(e)} disabled={this.props.loading}>
                            İrəli<Icon type={this.props.loading === true ? "loading" : "right"} />
                        </Button>
                    </Button.Group>
                </Row>
            </React.Fragment>
        )
    }
}

const IdentityForm = Form.create()(IdForm);

export default IdentityForm