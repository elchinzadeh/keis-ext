import React from 'react'
import {Col, Row, Form, Icon, Input, Divider, DatePicker, Select, InputNumber, Button, Radio} from 'antd';
import moment from "moment";
import ProfileImage from './profileImage'
import HelperApi from "../../../api/helperApi";

const FormItem = Form.Item;
const SelectOption = Select.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const InputGroup = Input.Group;

class PersonalInfo extends React.Component {

    state = {
        countries: [],
        districtCities: [],
    };

    componentDidMount() {
        this.loadCountryList();
        this.loadDistrictCityList();
    }

    loadCountryList = () => {
        HelperApi.getlistCountry().then(response => {
            if (response.data.error === null) {
                this.setState({
                    countries: response.data.data.entities
                });
            } else {
                this.setState({
                    error: response.data.error.message
                });
            }
        }).catch(error => {
            this.setState({
                error: error.message
            });
        });

    };

    loadDistrictCityList = () => {

        HelperApi.getDistrictCities().then(response => {
            if (response.data.error === null) {
                this.setState({
                    districtCities: response.data.data.entities
                });
            } else {
                this.setState({
                    error: response.data.error.message
                });
            }
        }).catch(error => {
            this.setState({
                error: error.message
            });
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.setPersonalInfo(values)
            }
        });
    };

    render() {

        const {userData, form, prevStep} = this.props;
        const {countries, districtCities} = this.state;

        const formItemLayout = {
            labelCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 12
                },
                lg: {
                    span: 12
                }
            },
            wrapperCol: {
                sm: {
                    span: 24
                },
                xs: {
                    span: 24
                },
                md: {
                    span: 12
                },
                lg: {
                    span: 12
                }
            }
        };
        const {getFieldDecorator} = form;

        return (
            <React.Fragment>
                <Row>
                    <Divider>
                        <h2 className="title">Şəxsi məlumatlar</h2>
                    </Divider>
                </Row>
                <Form layout="horizontal" onSubmit={(e) => this.handleSubmit(e)}>
                    <Row>
                        <ProfileImage image={this.props.userData.image} setProfileImage={this.props.setProfileImage}/>
                    </Row>
                    <Row>
                        <Col md={10} lg={10} xs={24} sm={24}>
                            <FormItem
                                label="Ad"
                                {...formItemLayout}>
                                {getFieldDecorator('firstName', {
                                    initialValue: userData ? userData.firstName : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa adınızı daxil edin'}],
                                })(
                                    <Input placeholder="Könüllünün adı"/>
                                )}

                            </FormItem>
                            <FormItem
                                label="Soyad"
                                {...formItemLayout}>
                                {getFieldDecorator('lastName', {
                                    initialValue: userData ? userData.lastName : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa soyadınızı daxil edin'}],
                                })(
                                    <Input placeholder="Könüllünün soyadı"/>
                                )}
                            </FormItem>
                            <FormItem
                                label="Ata adı"
                                {...formItemLayout}>
                                {getFieldDecorator('patronymic', {
                                    initialValue: userData ? userData.patronymic : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa ata adını daxil edin'}],
                                })(
                                    <Input placeholder="Könüllünün ata adı"/>
                                )}
                            </FormItem>
                            <FormItem
                                label="Cinsi"
                                {...formItemLayout}>
                                {getFieldDecorator('sex', {
                                    initialValue: userData ? userData.sex : true,
                                    rules: [{required: true, message: 'Zəhmət olmasa cinsinizi seçin'}],
                                })(
                                    <RadioGroup>
                                        <RadioButton value={true}>Kişi</RadioButton>
                                        <RadioButton value={false}>Qadın</RadioButton>
                                    </RadioGroup>
                                )}
                            </FormItem>
                            <FormItem
                                label="Doğulduğu yer"
                                {...formItemLayout}>
                                {getFieldDecorator('birthPlace', {
                                    initialValue: userData ? userData.birthPlace : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa doğum yerinizi daxil edin'}],
                                })(
                                    <Input placeholder="Könüllünün doğum yeri"/>
                                )}
                            </FormItem>
                            <FormItem
                                label="Milliyəti"
                                {...formItemLayout}>
                                {getFieldDecorator('nationality', {
                                    initialValue: userData ? userData.nationality : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa milliyətinizi daxil edin'}],
                                })(
                                    <Input placeholder="Könüllünün milliyəti"/>
                                )}
                            </FormItem>
                            <FormItem
                                label="Vətəndaşlığı"
                                {...formItemLayout}>
                                {getFieldDecorator('citizenshipCountryId', {
                                    initialValue: userData ? userData.citizenshipCountryId ? userData.citizenshipCountryId.id : undefined : undefined,
                                    rules: [{required: true, message: 'Zəhmət olmasa vətəndaşı olduğunuz ölkəni seçin'}],
                                })(
                                    <Select showSearch={true}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                            style={{width: '100%'}}
                                            placeholder="Seçilməyib">
                                        {
                                            countries ? countries.map(item => {
                                                return <SelectOption key={item.id}
                                                                     value={item.id}>{item.name}</SelectOption>
                                            }) : null
                                        }
                                    </Select>
                                )}
                            </FormItem>
                        </Col>
                        <Col md={10} lg={10} xs={24} sm={24}>
                            <FormItem
                                label="Doğum tarixi"
                                {...formItemLayout}>
                                {getFieldDecorator('birthDate', {
                                    initialValue: userData ? moment(userData.birthDate) : moment(),
                                    rules: [{required: true, message: 'Zəhmət olmasa doğum tarixinizi seçin'}],
                                })(
                                    <DatePicker style={{width: '100%'}} format="DD.MM.YYYY"
                                                placeholder="Könüllünün doğum tarixi"/>
                                )}
                            </FormItem>
                            <FormItem
                                label="Ailə vəziyyəti"
                                {...formItemLayout}>
                                {getFieldDecorator('maritalStatus', {
                                    initialValue: userData ? userData.maritalStatus : undefined,
                                    rules: [{required: true, message: 'Zəhmət olmasa ailə vəziyyətinizi seçin seçin'}],
                                })(
                                    <Select style={{width: '100%'}}
                                            placeholder="Seçilməyib">
                                        <SelectOption value={0}>Subay</SelectOption>
                                        <SelectOption value={1}>Evli</SelectOption>
                                    </Select>
                                )}
                            </FormItem>
                            <FormItem
                                label="Qeydiyyatda olduğu şəhər"
                                {...formItemLayout}>
                                {getFieldDecorator('registeredPlaceDistrictCityId', {
                                    initialValue: userData ? userData.registeredPlaceDistrictCityId ? userData.registeredPlaceDistrictCityId.id : undefined : undefined,
                                    rules: [{
                                        required: true,
                                        message: 'Zəhmət olmasa qeydiyyatda olduğunuz şəhəri seçin'
                                    }],
                                })(
                                    <Select showSearch={true}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                            style={{width: '100%'}}
                                            placeholder="Seçilməyib">
                                        {
                                            districtCities ? districtCities.map(item => {
                                                return <SelectOption key={item.id}
                                                                     value={item.id}>{item.name}</SelectOption>
                                            }) : null
                                        }
                                    </Select>
                                )}
                            </FormItem>
                            <FormItem
                                label="Qeydiyyat ünvanı"
                                {...formItemLayout}>
                                {getFieldDecorator('registeredPlaceAddress', {
                                    initialValue: userData ? userData.registeredPlaceAddress : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa qeydiyyat ünvanınızı daxil edin'}],
                                })(
                                    <Input placeholder="Könüllünün qeydiyyat ünvanı"/>
                                )}
                            </FormItem>
                            <FormItem
                                label="Yaşadığı şəhər"
                                {...formItemLayout}>
                                {getFieldDecorator('livingPlaceDistrictCityId', {
                                    initialValue: userData ? userData.livingPlaceDistrictCityId ? userData.livingPlaceDistrictCityId.id : undefined : undefined,
                                    rules: [{required: true, message: 'Zəhmət olmasa yaşadığınız şəhəri seçin'}],
                                })(
                                    <Select showSearch={true}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                            style={{width: '100%'}}
                                            placeholder="Seçilməyib">
                                        {
                                            districtCities ? districtCities.map(item => {
                                                return <SelectOption key={item.id}
                                                                     value={item.id}>{item.name}</SelectOption>
                                            }) : null
                                        }
                                    </Select>
                                )}
                            </FormItem>
                            <FormItem
                                label="Yaşayış ünvanı"
                                {...formItemLayout}>
                                {getFieldDecorator('livingPlaceAddress', {
                                    initialValue: userData ? userData.livingPlaceAddress : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa yaşayış ünvanınızı daxil edin'}],
                                })(
                                    <Input placeholder="Könüllünün yaşayış ünvanı"/>
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                    <Divider>Əlaqə məlumatları</Divider>
                    <Row>
                        <Col md={10} lg={10} xs={24} sm={24}>
                            <FormItem
                                label="Mobil telefon"
                                {...formItemLayout}>
                                {getFieldDecorator('phoneMobile', {
                                    initialValue: userData ? userData.phoneMobile : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa mobil nömrənizi daxil edin'}],
                                })(
                                    <Input
                                        addonBefore="+994"
                                        type="number"
                                        placeholder="Könüllünün mobil nömrəsi"
                                    />
                                )}
                            </FormItem>
                            <FormItem
                                label="Ev telefonu"
                                {...formItemLayout}>
                                {getFieldDecorator('phoneHouse', {
                                    initialValue: userData ? userData.phoneHouse : null,
                                    rules: [{required: true, message: 'Zəhmət olmasa ev nömrənizi daxil edin'}],
                                })(
                                    <Input
                                        addonBefore="+994"
                                        type="number"
                                        placeholder="Könüllünün ev nömrəsi"
                                    />                                )}
                            </FormItem>
                        </Col>
                        <Col md={10} lg={10} xs={24} sm={24}>
                            <FormItem
                                label="Facebook ünvanı"
                                {...formItemLayout}>
                                <InputGroup compact>
                                    {getFieldDecorator('facebookProfileUrl', {
                                        initialValue: userData ? userData.facebookProfileUrl : null,
                                        rules: [{
                                            required: true,
                                            message: 'Zəhmət olmasa facebook ünvanınızı daxil edin'
                                        }],
                                    })(
                                        <Input style={{width: '80%'}} placeholder="http://facebook.com/zuck"/>
                                    )}
                                    <Button style={{width: '20%'}} icon="link"
                                            href={userData ? userData.facebookProfileUrl : null}
                                            target="_blank"/>
                                </InputGroup>
                            </FormItem>
                        </Col>
                    </Row>
                    <Divider>Digər məlumatlar</Divider>
                    <Row>
                        <Col md={10} lg={10} xs={24} sm={24}>
                            <FormItem label="Hərbi mükəlləfiyyət" {...formItemLayout}>
                                {getFieldDecorator('militaryAttitude', {
                                    initialValue: userData ? userData.militaryAttitude : null,
                                    rules: [{
                                        required: true,
                                        message: 'Hərbi mükəlləfiyyətə münasibətinizi daxil edin'
                                    }],
                                })(
                                    <Input placeholder="Hərbi mükəlləfiyyətə münasibətiniz"/>
                                )}
                            </FormItem>
                            <FormItem label="Sürücülük vəsiqəsi" {...formItemLayout}>
                                {getFieldDecorator('haveDriveLicense', {
                                    initialValue: userData ? userData.haveDriveLicense : false,
                                    rules: [{required: false, message: 'Sürücülük vəsiqəsi'}],
                                })(
                                    <RadioGroup>
                                        <RadioButton value={true}>Var</RadioButton>
                                        <RadioButton value={false}>Yoxdur</RadioButton>
                                    </RadioGroup>
                                )}
                            </FormItem>
                        </Col>
                        <Col md={10} lg={10} xs={24} sm={24}>
                            <FormItem label="Fiziki məhdudiyyət" {...formItemLayout}>
                                {getFieldDecorator('physicallyLimitation', {
                                    initialValue: userData ? userData.physicallyLimitation : null,
                                    rules: [{required: false, message: 'Fiziki məhdudiyyətinizi daxil edin'}],
                                })(
                                    <Input placeholder="Fiziki məhdudiyyət"/>
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                    <Row type="flex" justify="center">
                        <Button.Group>
                            <Button onClick={() => prevStep()}>
                                <Icon type="left"/>Geri
                            </Button>
                            <Button type="primary" onClick={e => this.handleSubmit(e)}>
                                İrəli<Icon type="right"/>
                            </Button>
                        </Button.Group>
                    </Row>
                </Form>
            </React.Fragment>
        )
    }
}

const PersonalInfoForm = Form.create()(PersonalInfo);

export default PersonalInfoForm