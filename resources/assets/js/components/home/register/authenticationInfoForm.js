import React from 'react'
import {Col, Row, Form, Icon, Input, Divider, Button} from 'antd';

const FormItem = Form.Item;

class AuthForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err){
                this.props.setAuthInfo(values)
            }
        });
    };

    render() {
        const { prevStep } = this.props;
        const { getFieldDecorator } = this.props.form;

        return(
            <React.Fragment>
                <Row>
                    <Divider>
                        <h2 className="title">Hesab məlumatları</h2>
                    </Divider>
                </Row>
                <Row>
                    <Col md={{span: 6, offset: 9}}>
                        <Form onSubmit={this.handleSubmit} className="login-form">
                            <FormItem>
                                {getFieldDecorator('email', {
                                    rules: [{ required: true, message: 'Zəhmət olmasa e-poçt adresinizi daxil edin' }],
                                })(
                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="email" placeholder="E-poçt" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Zəhmət olmasa hesab şifrənizi daxil edin' }],
                                })(
                                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Şifrə" />
                                )}
                            </FormItem>
                        </Form>
                    </Col>
                </Row>
                <Row type="flex" justify="center">
                    <Button.Group>
                        <Button onClick={() => prevStep()}>
                            <Icon type="left" />Geri
                        </Button>
                        <Button type="primary" onClick={e => this.handleSubmit(e)}>
                            Qeydiyyatı tamamla<Icon type="right" />
                        </Button>
                    </Button.Group>
                </Row>
            </React.Fragment>
        )
    }
}

const AuthenticationForm = Form.create()(AuthForm);

export default AuthenticationForm