import React, {Component} from 'react';
import {List, Popconfirm, Button, Badge, Icon, Col, Row, Divider} from 'antd';
import moment from 'moment';
import HelperApi from '../../../api/helperApi';

import EducationForm from './educationInfoModal';

export default class EducationInfo extends Component {

    constructor() {
        super();
        this.state = {
            fetching: false,
            posting: false,
            entities: [],
            selectedEntity: null,
            totalCount: 0,
            modalOpen: false,

            organizations: [],
            levels: [],
            professions: []
        };
    }

    componentDidMount() {
        this.getOrganizations();
        this.getEducationLevels();
        this.getProfessions();
    }

    getOrganizations = () => {

        HelperApi.getEducationOrganizations().then(response => {

            if (response.data.error === null) {
                this.setState({
                    organizations: response.data.data.entities
                });
            }

        }).catch();

    };

    getEducationLevels = () => {

        HelperApi.getEducationLevels().then(response => {

            if (response.data.error === null) {
                this.setState({
                    levels: response.data.data.entities
                });
            }

        }).catch();

    };

    getProfessions = () => {

        HelperApi.getProfessions().then(response => {

            if (response.data.error === null) {
                this.setState({
                    professions: response.data.data.entities
                });
            }

        }).catch();

    };

    showModal = () => {
        this.setState({modalOpen: true, selectedEntity: null});
    };

    handleCancel = () => {
        this.setState({modalOpen: false});
    };

    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (!err) {
                this.props.setEducationInfo(values);
                this.handleCancel();
                this.formRef.props.form.resetFields()
            }
        });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    render() {
        const {prevStep, nextStep,  eduList} = this.props;
        const educationForms = {1:'Əyani', 2:'Qiyabi', 3:'Distant'};
        return (
            <React.Fragment>
                <Divider>Təhsil məlumatları</Divider>
                <EducationForm
                    wrappedComponentRef={this.saveFormRef}
                    educationInfo={this.state.selectedEntity}
                    organizations={this.state.organizations}
                    professions={this.state.professions}
                    levels={this.state.levels}
                    visible={this.state.modalOpen}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}/>
                <Row>
                    <Col md={{span: 14, push: 5}} lg={{span: 14, push: 5}} xs={24} sm={24} style={{textAlign: 'right'}}>
                        <Button type="primary" onClick={this.showModal}>
                            <Icon type="plus-circle-o"/>Əlavə et
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col md={{span: 14, push: 5}} lg={{span: 14, push: 5}} xs={24} sm={24}>
                        <List
                            loading={this.state.fetching}
                            itemLayout="horizontal"
                            dataSource={eduList}
                            locale={{emptyText: 'Təhsil məlumatı əlavə etmək üçün əlavə et düyməsinə klikləyin'}}
                            renderItem={item => (
                                <List.Item actions={[
                                    <Popconfirm title="Təhil məlumatını silməyə əminsinizmi?"
                                                onConfirm={() => this.props.deleteEducationInfo(item.id)}
                                                okText="Bəli" cancelText="Xeyr">
                                        <a style={{color: 'red'}}>Sil</a>
                                    </Popconfirm>
                                ]}>

                                    <List.Item.Meta
                                        title={<a onClick={(e) => { e.preventDefault(); }}>
                                            {this.state.organizations.length > 0 && this.state.organizations.find(org => {
                                                return org.id === +item.educationOrganizationId.id
                                            }).name}
                                        &nbsp;<Badge count={educationForms[item.educationForm]} style={{backgroundColor: '#52c41a'}}/> </a>}
                                        description={<span>
                                            <span>
                                                {
                                                    this.state.professions.length > 0 && this.state.professions.find(profession => {
                                                        return profession.id === +item.professionId.id
                                                    }).name
                                                }
                                            </span>
                                            , &nbsp;
                                            <span>
                                                {
                                                    this.state.levels.length > 0 && this.state.levels.find(level => {
                                                        return level.id === +item.educationLevelId.id
                                                    }).name
                                                }
                                            </span>
                                        </span>
                                        }
                                    />
                                    <div>
                                        {moment(item.startDate).format('LL')}
                                        &nbsp;-&nbsp;
                                        {item.endDate === null ? 'Davam edir' : moment(item.endDate).format('LL')}
                                    </div>
                                </List.Item>
                            )}
                        />
                    </Col>
                </Row>
                <Row type="flex" justify="center">
                    <Button.Group>
                        <Button onClick={() => prevStep()}>
                            <Icon type="left" />Geri
                        </Button>
                        <Button type="primary" onClick={() => nextStep()}>
                            İrəli<Icon type="right" />
                        </Button>
                    </Button.Group>
                </Row>
            </React.Fragment>
        );
    }
}

