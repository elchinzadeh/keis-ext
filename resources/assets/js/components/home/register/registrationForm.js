import React from 'react';
import cookie from 'react-cookies'
import {Card, message} from 'antd';

// Components
import CitizenshipSelection from './citizenshipSelection'
import IdentityInfoForm from './identityInfoForm'
import PersonalInfoForm from './personalInfoForm'
import EducationInfo from "./educationInfo";
import AuthenticationInfoForm from "./authenticationInfoForm";

import RegistrationApi from "../../../api/registrationApi";

export default class RegistrationForm extends React.Component {

    constructor() {
        super();

        this.state = {
            step: 0,
            citizenship: null,
            userData: {
                eduList: []
            },
            idCardNo: null,
            idCardFin: null,
            identityLoading: false
        }

    }

    nextStep = () => {
        if (this.state.citizenship === 1 && this.state.step === 0){
            this.setState({
                step: this.state.step + 2
            })
        }else {
            this.setState({
                step: this.state.step + 1
            })
        }

    };

    prevStep = () =>  {
        if (this.state.citizenship === 1 && this.state.step === 2){
            this.setState({
                step: this.state.step - 2
            })
        }else {
            this.setState({
                step: this.state.step - 1
            })
        }
    };

    setCitizenship = (citizenshipId) => {
        this.setState({
            citizenship: citizenshipId,
        }, () => {
            this.nextStep()
        });

    };

    setIdentityInfo = (identityInfo) => {
        this.setState({
            idCardNo: identityInfo.idCardNo,
            idCardFin: identityInfo.idCardFin,
            identityLoading: true
        });
        this.getDataFromIAMAS(identityInfo.idCardNo, identityInfo.idCardFin)
    };

    getDataFromIAMAS = (idCardNo, idCardFin) => {
        RegistrationApi.getIdCardData(idCardNo ,idCardFin).then(response => {
            if (!response.data.error) {
                this.setState({
                    identityLoading: false,
                    userData: {
                        ...this.state.userData,
                        ...response.data.data
                    }
                }, () => {this.nextStep()})
            }else{
                this.setState({identityLoading: false});
                message.error(response.data.error.message)
            }
        }).catch(error => {
            this.setState({identityLoading: false});
            message.error(error.message)
        })
    };

    setPersonalInfo = (values) => {
        this.setState({
            userData: {
                ...this.state.userData,
                ...values
            }
        }, () => {this.nextStep()})
    };

    setProfileImage = (image) => {
        this.setState({
            userData: {
                ...this.state.userData,
                image: image
            }
        })
    };

    setEducationInfo = (values) => {
        let eduList = this.state.userData.eduList;
        const body = {
            educationForm: values.educationForm,
            educationLevelId: {id: values.educationLevelId},
            educationOrganizationId: {id: values.educationOrganizationId},
            professionId: {id: values.professionId},
            startDate: values.dateRange[0].format('YYYY-MM-DD'),
            endDate: values.dateRange[1].format('YYYY-MM-DD'),
        };
        eduList.push(body);
        this.setState({
            eduList
        })
    };

    deleteEducationInfo = (id) => {
        let eduList = this.state.userData.eduList;
        for (let i = 0; i < eduList.length; i++) {
            if (eduList[i].id === id) {
                eduList.splice(i, 1);
                break
            }
        }
        this.setState({
            userData: {
                ...this.state.userData,
                eduList
            }
        })
    };

    setAuthInfo = (values) => {
        this.setState({
            userData: {
                ...this.state.userData,
                ...values
            }
        }, () => {this.submitRegistrationInfo()})
    };

    submitRegistrationInfo = () =>  {
        const {idCardNo, idCardFin, firstName, lastName, patronymic, birthDate, birthPlace, maritalStatus, sex, nationality, citizenshipCountryId,
            registeredPlaceDistrictCityId, livingPlaceDistrictCityId, registeredPlaceAddress, livingPlaceAddress,
            facebookProfileUrl, physicallyLimitation, militaryAttitude, haveDriveLicense, phoneHouse, phoneMobile, email, password, eduList, image} = this.state.userData;

        const isAze = this.state.citizenship === 0;
        const body = {
            isAze: isAze,
            idCardNo: idCardNo,
            idCardFin: idCardFin,
            image: image,
            firstName: firstName,
            lastName: lastName,
            patronymic: patronymic,
            birthDate: birthDate,
            birthPlace: birthPlace,
            maritalStatus: maritalStatus,
            sex: sex,
            nationality: nationality,
            citizenshipCountryId: {
                id: citizenshipCountryId
            },
            registeredPlaceDistrictCityId: {
                id: registeredPlaceDistrictCityId
            },
            livingPlaceDistrictCityId: {
                id: livingPlaceDistrictCityId
            },
            registeredPlaceAddress: registeredPlaceAddress,
            livingPlaceAddress: livingPlaceAddress,
            facebookProfileUrl: facebookProfileUrl,
            physicallyLimitation: physicallyLimitation,
            militaryAttitude: militaryAttitude,
            haveDriveLicense: haveDriveLicense,
            phoneHouse: phoneHouse,
            phoneMobile: phoneMobile,
            email: email,
            password: password,
            eduList: eduList
        };
        RegistrationApi.registration(body).then(response => {
            if (response.data.error === null){
                message.success('Qeydiyyat tamamlandı');
                cookie.save('ut', response.data.data, { path: '/' });
                window.location.replace('dashboard')
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    render () {
        const step = {
            0: <CitizenshipSelection setCitizenship={this.setCitizenship}/>,
            1: <IdentityInfoForm step={this.state.step}
                                 citizenship={this.state.citizenship}
                                 idCardNo={this.state.idCardNo}
                                 idCardFin={this.state.idCardFin}
                                 prevStep={this.prevStep}
                                 setIdentityInfo={this.setIdentityInfo}
                                 loading={this.state.identityLoading}/>,
            2: <PersonalInfoForm step={this.state.step}
                                 citizenship={this.state.citizenship}
                                 prevStep={this.prevStep}
                                 userData={this.state.userData}
                                 setPersonalInfo={this.setPersonalInfo}
                                 setProfileImage={this.setProfileImage}/>,
            3: <EducationInfo step={this.state.step}
                              citizenship={this.state.citizenship}
                              prevStep={this.prevStep}
                              nextStep={this.nextStep}
                              setEducationInfo={this.setEducationInfo}
                              deleteEducationInfo={this.deleteEducationInfo}
                              eduList={this.state.userData.eduList}/>,
            4: <AuthenticationInfoForm prevStep={this.prevStep}
                                       setAuthInfo={this.setAuthInfo}/>
        };

        return (
            <div className="registrationForm">
                <Card bordered={false}>
                    {
                        step[this.state.step]
                    }
                    {/*<Row type="flex" justify="center">*/}
                        {/*<Button.Group>*/}
                            {/*<Button type="primary" disabled={this.state.step === 0} onClick={() => this.prevStep()}>*/}
                                {/*<Icon type="left" />Geri*/}
                            {/*</Button>*/}
                            {/*<Button type="primary" disabled={this.state.step === 4 || this.state.citizenship === null} onClick={() => this.nextStep()}>*/}
                                {/*İrəli<Icon type="right" />*/}
                            {/*</Button>*/}
                        {/*</Button.Group>*/}
                    {/*</Row>*/}
                </Card>
            </div>
        )
    }
}