import React, { Component } from 'react';
import {Button, message, Upload} from 'antd'

import '../../profile/profileImage/profileImage.css'
import ProfileApi from "../../../api/profile/profileApi";

export default class ProfileImage extends Component {

    constructor() {
        super();

        this.state = {
            fileList: []
        }
    }

    convertBase64 = (file) => {
        let base64file = null;
        if (/\.(jpe?g|png)$/i.test(file.name)) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = () => {
                base64file = reader.result;
                this.props.setProfileImage(base64file)
            };
            reader.onerror = function (error) {
                message.error(error);
            };
        }else{
            message.error('Zəhmət olmasa .jpg və ya .png uzantılı fayl seçin')
        }
    };

    render() {
        const uploadProps = {
            beforeUpload: (file) => {
                const isLt2M = file.size / 1024 / 1024 < 2;
                if (!isLt2M) {
                    message.error('Şəkilin həcmi 2MB-dan az olmalıdır');
                }else{
                    this.setState({
                        fileList: [file],
                    });
                    this.convertBase64(file);
                }
                return false;
            },
            showUploadList: false
        };

        return(
            <div className="profile-image">
                <div className="image" style={{backgroundImage: this.props.image
                        ? this.props.image.substring(0,4) !== 'data'
                            ? 'url(data:image/jpeg;base64,'+this.props.image+')'
                            : 'url('+this.props.image+')'
                        : 'url("https://netbranding.co.nz/wp-content/uploads/2014/05/avatar-1.png")'
                }} />

                <div className="edit-btn">
                    <Upload {...uploadProps}>
                        <Button type="normal" shape="circle" icon="edit" size={'small'}/>
                    </Upload>
                </div>
            </div>
        )
    }
}