import React from 'react'
import {Button, Form, Icon, Input, message} from "antd";
import AuthApi from "../../api/authApi";
import cookie from "react-cookies";

const FormItem = Form.Item;

class ForgotPasswordForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const body = {
                    email: values.email,
                    password: values.password
                };
                AuthApi.login(body).then(response => {
                    if (response.data.error === null) {
                        cookie.save('ut', response.data.data.token, { path: '/' });
                        cookie.save('itl', response.data.data.isTeamLeader, { path: '/' });
                        window.location.replace('/dashboard');
                    }else {
                        message.error(response.data.error.message)
                    }
                });
            }
        });
    };

    render () {
        const { getFieldDecorator } = this.props.form;

        return (
            <Form onSubmit={this.handleSubmit} className="login-form">
                <h1>Şifrəni unutmusunuz?</h1>
                <FormItem>
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Zəhmət olmasa, emailinizi daxil edin' }],
                    })(
                        <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
                    )}
                </FormItem>
                <FormItem>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Şifrəni yenilə
                    </Button>
                    Və ya <a onClick={this.props.forgotPassword}>Daxil ol</a>
                    <a className="login-form-forgot" onClick={this.props.forgotPassword}>Şifrəni unutmamışam</a>
                </FormItem>
            </Form>
        )
    }
}

const ForgotPassword = Form.create()(ForgotPasswordForm);

export default ForgotPassword