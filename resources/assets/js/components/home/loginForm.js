import React, {Component} from 'react';
import {Form, Icon, Input, Button, Checkbox, message} from 'antd';
import cookie from 'react-cookies';
import AuthApi from "../../api/authApi";

const FormItem = Form.Item;

class NormalLoginForm extends Component {

    state = {
        isTeamLeader: null
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const body = {
                    email: values.email,
                    password: values.password
                };
                AuthApi.login(body).then(response => {
                    if (response.data.error === null) {
                        cookie.save('ut', response.data.data.token, { path: '/' });
                        cookie.save('itl', response.data.data.isTeamLeader, { path: '/' });
                        window.location.replace('/dashboard');
                    }else {
                        message.error(response.data.error.message)
                    }
                });
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} className="login-form">
                <h1>Daxil ol</h1>
                <FormItem>
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: 'Zəhmət olmasa, emailinizi daxil edin' }],
                    })(
                        <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Zəhmət olmasa, şifrənizi daxil edin' }],
                    })(
                        <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Şifrə" />
                    )}
                </FormItem>
                <FormItem>
                    <a className="login-form-forgot" onClick={this.props.forgotPassword}>Şifrəni unutmuşam</a>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Daxil ol
                    </Button>
                    Və ya <a href="/registration">qeydiyyatdan keç!</a>
                </FormItem>
            </Form>
        );
    }
}

const LoginForm = Form.create()(NormalLoginForm);

export default LoginForm;