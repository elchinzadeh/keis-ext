import React, {Component} from 'react';
import { Col } from 'antd';
import LoginForm from './loginForm';

export default class ExtLogin extends Component {

    render() {
        return (
            <div className="page-wrapper">
                <Col sm={24} xl={12}>
                    <div className="page-left">
                        <div className="logo-container">
                            <img src="../img/logo-w.png" className="logo" alt=""/>
                        </div>
                        <div className="content">
                            <h1>ASAN Könüllülük Məktəbi</h1>
                            <p>
                                Azərbaycan Respublikasının Prezidenti yanında Vətəndaşlara Xidmət və Sosial İnnovasiyalar üzrə Dövlət Agentliyinin tabeliyində
                                olan “ASAN xidmət” mərkəzlərində könüllü fəaliyyət göstərmək istəyən şəxslər ilk növbədə qeydiyyatdan keçməlidir.
                            </p>
                        </div>
                    </div>
                </Col>
                <Col sm={24} xl={12}>
                    <div className="page-right">
                        <div className="right-form-container">
                            <LoginForm/>
                        </div>
                    </div>
                </Col>
            </div>
        );
    }
}