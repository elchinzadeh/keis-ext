import React, {Component} from 'react';
import AppRouter from './routes/appRouter';
import moment from 'moment';
import 'moment/locale/az';

class App extends Component {
    render() {
        return <AppRouter/>;
    }
}

export default App;
