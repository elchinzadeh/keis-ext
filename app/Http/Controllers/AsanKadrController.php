<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class AsanKadrController extends Controller
{

    private $headers = [
        'Accept-Language' => 'az',
        'Content-Type' => 'application/json',
    ];

    public function __construct()
    {
        $this->client = new Client();
    }

    public function handleGet(Request $request, $uri)
    {
        $url = rtrim(env('APP_API_URL', 'default').$request->getRequestUri(), '/');

        try {
            $response = $this->client->get($url, [
                'headers' => $this->headers
            ]);

            $response = $response->getBody()->getContents();

            $response = json_decode($response);

            return response()->json($response);
        }
        catch (ClientException $e) {
            echo '<h1>'.$e->getResponse()->getStatusCode().' '.$e->getResponse()->getReasonPhrase().'</h1>';
        }



    }
}
