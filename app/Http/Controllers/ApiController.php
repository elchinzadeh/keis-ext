<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \GuzzleHttp\Client;

class ApiController extends Controller
{
    /**
     * @var Client
     */
    private $client;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleGet(Request $request)
    {
        $url = rtrim(env('APP_API_URL', 'default').$request->get('uri'), '/');

        $response = $this->client->get($url, [
            'headers' => [
                'Authorization' => $request->headers->get('authorization'),
                'Content-Type' => $request->headers->get('content-type'),
            ]
        ]);

        $response = $response->getBody()->getContents();

        $response = json_decode($response);

        return response()->json($response);

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function handlePost(Request $request)
    {
        $url = rtrim(env('APP_API_URL', 'default').$request->get('uri'), '/');
        $data = $request->all();
        array_pop($data);
//        dd($data);

        $response = $this->client->post($url, [
            'json' => $data,
            'headers' => [
                'Authorization' => $request->headers->get('authorization'),
                'Content-Type' => $request->headers->get('content-type'),
            ]
        ]);


        $response = $response->getBody()->getContents();

        $response = json_decode($response);

        return response()->json($response);
    }
}
