<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', 'ApiController@handleGet');
Route::post('/', 'ApiController@handlePost');
Route::get('/asanKadr/{uri}', 'AsanKadrController@handleGet');
//Route::get('/asanKadr/checkEmail', 'AsanKadrController@checkEmail');
//Route::get('/asanKadr/fileTransfer', 'AsanKadrController@fileTransfer');
//Route::get('/asanKadr/getDataByToken', 'AsanKadrController@getDataByToken');
